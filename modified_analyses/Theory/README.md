# Theory

Library of SM Theory proedictions in yoda format (/THY/).

## Status/wish list

### 13 TeV 

In some approximate priority order...

#### ATLAS_2018_I1634970 

ATLAS Inclusive jet and dijet cross section measurement at sqrt(s) = 13 TeV

#### CMS_2016_I1459051 

Measurement of the inclusive jet cross-section in pp collisions at $\sqrt{s} = 13 TeV

#### CMS_2018_I1682495

Jet mass in dijet events in $pp$ collisions at 13 TeV

#### ATLAS_2017_I1514251:LMODE=EL 

Z plus jets at 13 TeV

#### ATLAS_2017_I1514251:LMODE=MU 

Z plus jets at 13 TeV

#### CMS_2017_I1610623 

Measurements of differential cross sections for the associated production of a W boson and jets in proton-proton collisions at sqrt(s) = 13 TeV

#### CMS_2016_I1491950 

Differential cross sections for top quark pair production using the lepton+jets final state in proton proton collisions at 13 TeV

#### CMS_2018_I1662081 

Measurement of the differential cross sections of top quark pair production as a function of kinematic event variables in pp collisions at sqrt(s) = 13 TeV

#### CMS_2018_I1663958 

Differential cross sections for top quark pair production using the lepton+jets final state in proton proton collisions at 13 TeV

#### ATLAS_2017_I1614149 

Resolved and boosted ttbar l+jets cross sections at 13 TeV

#### ATLAS_2017_I1625109 

Measurement of $ZZ -> 4\ell$ production at 13 TeV


#### ATLAS_2019_I1718132:LMODE=ELMU 

Control region measurements for leptoquark search at 13 TeV

#### ATLAS_2019_I1718132:LMODE=ELEL 

Control region measurements for leptoquark search at 13 TeV

#### ATLAS_2019_I1718132:LMODE=MUMU 

Control region measurements for leptoquark search at 13 TeV

#### ATLAS_2016_I1458270 

0-lepton SUSY search with 3.2/fb of 13 TeV $pp$ data

#### ATLAS_2018_I1646686 

All-hadronic boosted ttbar at 13 TeV

#### ATLAS_2018_I1656578 

Differential $t\bar{t}$ $l$+jets cross-sections at 13~TeV

#### ATLAS_2018_I1705857 

ttbb at 13 TeV

#### ATLAS_2017_I1645627 

Isolated photon + jets at 13 TeV


## Ones we already have

(better/alternative calculations also welcome!)

### 13 TeV

#### ATLAS_2019_I1720442 

Inclusive 4-lepton lineshape at 13 TeV.

Sherpa+NLO from https://www.hepdata.net/record/ins1720442?version=1

#### ATLAS_2017_I1609448

$p_T^\text{miss}$+jets cross-section ratios at 13 TeV

Prediction provided in the paper, from Sherpa with an NLO scaling.