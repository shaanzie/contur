SHELL := /bin/bash
DBDIR = AnalysisTools/contur/contur/data
ANALYSES = modified_analyses
GRIDPACK = AnalysisTools/GridSetup/GridPack

all: $(DBDIR)/analyses.db $(ANALYSES)/Rivet-ConturOverload.so $(GRIDPACK)

$(ANALYSES)/Rivet-ConturOverload.so : $(ANALYSES)/*
	$(ANALYSES)/buildrivet.sh

$(DBDIR)/analyses.db : $(DBDIR)/analyses.sql
	rm -f $@
	sqlite3 $@ < $<

$(GRIDPACK) :
	rm -rf $(GRIDPACK)
	mkdir $(GRIDPACK)
	contur-mkana -o $(GRIDPACK)
	@echo "INFO: You should source setupContur.sh again to update environment variables"

.PHONY :  $(DBDIR)/analyses.db

.PHONY :  $(GRIDPACK)

clean:
	@echo "Removing all generated files"
	@rm -f AnalysisTools/contur/contur/*.pyc
	@rm -f AnalysisTools/contur/contur/data/analysis.db
	@rm -f AnalysisTools/contur/contur/*/*.pyc
	@rm -f modified_analyses/*.so
	@rm -rf AnalysisTools/GridSetup/GridPack
