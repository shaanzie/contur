#!/usr/bin/env python

"""
TODO:
- Fix 'gen_gif' to read all '*.png's in directory and produce .gif
"""

from inspect import getmembers, isfunction
import importlib

import matplotlib
matplotlib.use('Agg')

from contur.factories.contur_depot import *
from contur.plot import *
from argparse import ArgumentParser, RawTextHelpFormatter
import sys



# opts, mapfiles = parser.parse_args()

parser_description = (
    "Plot contur data from a .map file.\n"
    "You should specify a .map file to read and 2 or 3 variable to plot\n"
    "\n"
    "Usage examples:\n"
    "---------------\n"
    "contur-plot myscan.map Xm Y1\n"
    "    --> to plot heat map with Xm on x-axis and Y1 on y-axis\n"
    "\n"
    "contur-plot myscan.map Xm Y1 gYq -s 50\n"
    "    --> to plot Xm on x-axis Y1 on y-axis and slice 50 of gYq (z-axis)\n"
    "\n"
    "contur-plot myscan.map Xm Y1 gYq -sc\n"
    "    --> to plot 3d scatter graph\n"
    "\n"
    "contur-plot myscan.map Xm Y1 gYq -a\n"
    "    --> to plot Xm on x-axis, Y1 on y-axis and all slices of gYq\n")

parser = ArgumentParser(description=parser_description,
                        formatter_class=RawTextHelpFormatter)
# Positional arguments
parser.add_argument('map_file', type=str, help=('Path to .map file '
                                                'containing list of conturDepot objects.'))
parser.add_argument('variables', nargs='*', type=str,
                    help=('x, y [and z] variables to plot.'))

# Optional arguments
parser.add_argument('-t', "--theory", type=str, default=None,
                    help="Python file with theory functions to load and plot")
parser.add_argument('-d', "--data", type=str, default=None,
                    help="Python file loading alternative datagrids")

parser.add_argument('-xl', "--xlog", action="store_true",
                    help="Set the xaxis to be displayed on a log scale")
parser.add_argument('-yl', "--ylog", action="store_true",
                    help="Set the yaxis to be displayed on a log scale")
# TODO: remove do-nothing option
parser.add_argument('-np', action='store_true',
                    help="Turn off individual analysis-pool plots (=default behaviour).")
parser.add_argument('--pools', dest="plot_pools", action='store_true',
                    help="Turn on plotting of individual analysis pools (much slower!)")
parser.add_argument('-O', '--omit', type=str,
                    help='Name of pool to omit (will slow things down!)', default="")

parser.add_argument('-o', '--output_path', type=str, default='conturPlot',
                    help=("Path to output plot(s) to."))
parser.add_argument('-x', '--xlabel', type=str, default=None,
                    help='x-axis label. Accepts latex formatting but special characters must be input with a slash, e.g. \$M\_\{z\'\}\$~\[GeV\]')
parser.add_argument('-y', '--ylabel', type=str, default=None,
                    help='y-axis label. Accepts latex formatting but special characters must be input with a slash, e.g. \$M\_\{z\'\}\$~\[GeV\]')
parser.add_argument('-sp', '--save_plots', action='store_true',
                    help="Save the raw matplotlib axes to a file for graphical manipulation")
parser.add_argument('-T', '--title', type=str,
                    help='Title for plot.', default="")
parser.add_argument('-L', '--ilevel', '--iLevel', type=int,
                    help='interpolation zoom level', default=3)
parser.add_argument('--style', dest="style", default="DRAFT", choices=[
                    "DRAFT", "FINAL"], type=str.upper, help="Global flag for plot-styling variations: 'final' will have no title or cmap key")
parser.add_argument('--isigma', '--iSigma', type=float,
                    help='interpolation smoothing radius, in mesh cells', default=0.75)
parser.add_argument('--num-dpools', dest="ndpools", type=int,
                    help='Number of levels of (sub)dominant pool plots to make.', default=1)
parser.add_argument('--clstxt', dest="showcls", default=False, action="store_true",
                    help="Write CLs values on top of the mesh in the detailed dominant-pool plots.")
parser.add_argument('--no-clsdpool', dest="simplecls", default=False, action="store_true",
                    help="Skip the detailed dominant-pool plot with lead/sub/diff CLs meshes.")
args = parser.parse_args()

parsing_error_msg = ("To call contur-plot you must specify an input .map "
                     "file and 2 or 3 variables to plot!\nThe format must "
                     "follow:\ncontur-plot .map_file x_variable "
                     "y_variable [z_variable] [optional_arguments]")

# Error catching
if len(args.variables) not in [2, 3]:
    print("Error parsing arguments!\n\n" + parsing_error_msg)
    sys.exit()


# Import a module containing functions to define extra contours
sys.dont_write_bytecode = True
DataFunctions = []
if args.data:
    # Work out the module path and name (with any .py stripped)
    if "/" not in args.data:
        args.data = os.path.join("./", args.data)
    moddir, modfile = os.path.split(args.data)
    modname = os.path.splitext(modfile)[0]
    # Set the import path as needed, and import the module from there
    sys.path.append(moddir)
    i = importlib.import_module(modname)
    DataFunctions = [o for o in getmembers(i) if isfunction(o[1])]
    # Alphabetically sort this list in place so the colors are consistently trackable
    DataFunctions.sort(key=lambda v: (v[0].upper(), v[0].islower()))
    print("Imported alternative data constraints from %s" % args.data)


# Run the conturPlot processing
with open(args.map_file, 'rb') as f:
    file = pickle.load(f)
    contur.util.mkoutdir(args.output_path)

    ctrPlot = ConturPlotBase(file,
                             outPath=args.output_path, plotTitle=args.title,
                             savePlotFile=args.save_plots, omittedPools=args.omit,
                             iLevel=args.ilevel, iSigma=args.isigma, clLevel=args.ndpools,
                             style=args.style, showcls=args.showcls, simplecls=args.simplecls)
    ctrPlot.doPlotPools = args.plot_pools
    if args.theory:
        ctrPlot.addTheory(TheoryFunctions)
    if args.data:
        ctrPlot.addData(DataFunctions)

    ctrPlot.buildAxesfromGrid(xarg=args.variables[0], yarg=args.variables[1], logX=args.xlog, logY=args.ylog,
                              xlabel=args.xlabel, ylabel=args.ylabel)
    if args.save_plots:
        ctrPlot.dumpPlotObjects()
    
    testfile = open("xcoords.txt", "w+")
    testfile.write(str(ctrPlot.mapAxis[args.variables[0]]))
    testfile.write("\n")
    testfile.close()
    testfile = open("ycoords.txt", "w+")
    testfile.write(str(ctrPlot.mapAxis[args.variables[1]]))
    testfile.write("\n")
    testfile.close()
    testfile = open("zcoords.txt", "w+")
    testfile.write(str(ctrPlot.conturGrid.T))
    testfile.write("\n")
    testfile.close()

    print("Done")
