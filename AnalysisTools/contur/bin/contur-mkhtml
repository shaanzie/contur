#! /usr/bin/env python

"""\
%prog [options] <yodafile1> [<yodafile2> <yodafile3>...] [PLOT:Key1=Val1:...]

Make web pages from histogram files written out by Rivet and data files from Contur.
(Adapted from rivet-mkhtml.)  

Reference data, analysis metadata, and plot style information should be found
automatically (if not, set the RIVET_ANALYSIS_PATH or similar variables
appropriately).

Contur data is assumed to be in the directory ./plots and ./ANALYSIS.
Output willbe written to ./contur-plots 

Any existing output directory will be overwritten.

ENVIRONMENT:
 * RIVET_ANALYSIS_PATH: list of paths to be searched for plugin
     analysis libraries at runtime
 * RIVET_DATA_PATH: list of paths to be searched for data files
"""

import shutil
import glob
import datetime
from rivet.util import htmlify
import yoda
from optparse import OptionParser, OptionGroup
from subprocess import Popen, PIPE
import rivet
import sys
import os

rivet.util.check_python_version()
rivet.util.set_process_name(os.path.basename(__file__))


parser = OptionParser(usage=__doc__)
parser.add_option("-d", "--outdir", dest="OUTPUTDIR",
                  default="contur-plots", help="directory where output will be written.")
parser.add_option("-n", "--num-threads", metavar="NUMTHREADS", dest="NUMTHREADS", type=int,
                  default=None, help="request make-plots to use a specific number of threads")
parser.add_option("--ignore-missing", dest="IGNORE_MISSING", action="store_true",
                  default=False, help="ignore missing YODA files")
parser.add_option("-i", "--ignore-unvalidated", dest="IGNORE_UNVALIDATED", action="store_true",
                  default=False, help="ignore unvalidated analyses")
parser.add_option("--no-cleanup", dest="NO_CLEANUP", action="store_true", default=False,
                  help="keep plotting temporary directory")
parser.add_option("--no-subproc", dest="NO_SUBPROC", action="store_true", default=False,
                  help="don't use subprocesses to render the plots in parallel -- useful for debugging")
parser.add_option("--pwd", dest="PATH_PWD", action="store_true", default=False,
                  help="append the current directory (pwd) to the analysis/data search paths (cf. $RIVET_ANALYSIS_PATH)")
parser.add_option("--rp","--reducedPlots", dest="REDUCEDPLOTS", action="store_true", default=False,
                  help="Only make a reduced number of plots: those which appear in the summary")

stygroup = OptionGroup(parser, "Style options")
stygroup.add_option("-t", "--title", dest="TITLE",
                    default="Contur Plots: Constraints On New Theories Using Rivet",
                    help="title to be displayed on the main web page")
stygroup.add_option("--reftitle", dest="REFTITLE",
                    default="Data", help="legend entry for reference data")
stygroup.add_option("--no-plottitle", dest="NOPLOTTITLE", action="store_true",
                    default=False, help="don't show the plot title on the plot "
                                        "(useful when the plot description should only be given in a caption)")
stygroup.add_option("--no-ratio", dest="SHOW_RATIO", action="store_false",
                    default=True, help="don't draw a ratio plot under each main plot.")
stygroup.add_option("--errs", "--mc-errs", dest="MC_ERRS", action="store_true",
                    default=False, help="plot error bars.")
stygroup.add_option("--offline", dest="OFFLINE", action="store_true",
                    default=False, help="generate HTML that does not use external URLs.")
stygroup.add_option("--pdf", dest="VECTORFORMAT", action="store_const", const="PDF",
                    default="PDF", help="use PDF as the vector plot format.")
stygroup.add_option("--ps", dest="VECTORFORMAT", action="store_const", const="PS",
                    default="PDF", help="use PostScript as the vector plot format. DEPRECATED")
stygroup.add_option("--booklet", dest="BOOKLET", action="store_true",
                    default=False, help="create booklet (currently only available for PDF with pdftk or pdfmerge).")
stygroup.add_option("--font", dest="OUTPUT_FONT", choices="palatino,cm,times,helvetica,minion".split(","),
                    default="palatino", help="choose the font to be used in the plots")
stygroup.add_option("--palatino", dest="OUTPUT_FONT", action="store_const", const="palatino", default="palatino",
                    help="use Palatino as font (default). DEPRECATED: Use --font")
stygroup.add_option("--cm", dest="OUTPUT_FONT", action="store_const", const="cm", default="palatino",
                    help="use Computer Modern as font. DEPRECATED: Use --font")
stygroup.add_option("--times", dest="OUTPUT_FONT", action="store_const", const="times", default="palatino",
                    help="use Times as font. DEPRECATED: Use --font")
stygroup.add_option("--helvetica", dest="OUTPUT_FONT", action="store_const", const="helvetica", default="palatino",
                    help="use Helvetica as font. DEPRECATED: Use --font")
stygroup.add_option("--minion", dest="OUTPUT_FONT", action="store_const", const="minion", default="palatino",
                    help="use Adobe Minion Pro as font. DEPRECATED: Use --font")
parser.add_option_group(stygroup)


vrbgroup = OptionGroup(parser, "Verbosity control")
vrbgroup.add_option("-v", "--verbose", help="add extra debug messages", dest="VERBOSE",
                    action="store_true", default=False)
parser.add_option_group(vrbgroup)

def writeSummary(summary, reduced_list_of_plots=[]):

    index.write("<p>")

    heading = True
    while heading == True:
        text = summary.readline()
        if "pools" in text:
            heading = False
            continue
        index.write("%s\n" % text)
        index.write("</br>")

    index.write("</p>")

    index.write("<h2>In each analysis pool, these plots contributed:</h2>\n")

    cl = 0.0
    for line in summary:
        list = line.split(",")
        if list[0][0] == "{":
            # This is a comment line
            continue
        for name in list:
            parts = name.split("/")
            if len(parts) == 1:
                try:
                    # this is the cl for this pool
                    cl = float(parts[0])
                    index.write("<p><em> Exclusion from this pool alone: " +
                                '{:5.2f}'.format(100*cl) + "% </em></p>")
                except:
                    current_pool = str(parts[0])
                    # This is a line identifying the analysis pool
                    # TODO would be good to add a human-readable anapool name to the DB.
                    index.write(
                        '<div style="float:none; overflow:auto; width:100%">\n')
                    index.write("<h3>Analysis Pool:" + current_pool + "</h3>")

            else:
                # This is a line identifying histograms
                #obsname = current_pool + "/" + str(parts[1]) + "/" + str(parts[2])
                obsname = current_pool + name
                datname=name.strip()+".dat"
                if not datname in reduced_list_of_plots: reduced_list_of_plots+=[datname]
                analysis = str(parts[0])
                pngfile = obsname + ".png"
                vecfile = obsname + "." + opts.VECTORFORMAT.lower()
                srcfile = obsname + ".dat"
                index.write(
                    '  <div style="float:left; font-size:smaller; font-weight:bold;">\n')
                index.write('    <a href="#%s-%s">&#9875;</a><a href="%s">&#8984</a> %s:<br/>\n' %
                            (current_pool+"/"+analysis, obsname, srcfile, os.path.splitext(vecfile)[0]))
                index.write('    <a name="%s-%s"><a href="%s">\n' %
                            (current_pool+"/"+analysis, obsname, vecfile))
                index.write('      <img src="%s">\n' % pngfile)
                index.write('    </a></a>\n')
                index.write('  </div>\n')

    summary.close()
    index.write('</div>\n')

#####################


opts, yodafiles = parser.parse_args()

# Add pwd to search paths
if opts.PATH_PWD:
    rivet.addAnalysisLibPath(os.path.abspath("."))
    rivet.addAnalysisDataPath(os.path.abspath("."))

# TODO: make this changeable, and ANALYSIS dir too
inputdir = "plots"
# Make the output directory
if os.path.exists(opts.OUTPUTDIR) and not os.path.realpath(opts.OUTPUTDIR) == os.getcwdu():
    import shutil
    shutil.rmtree(opts.OUTPUTDIR)
try:
    shutil.copytree(inputdir, opts.OUTPUTDIR)
except:
    print("Error: failed to copy to directory '%s'" % opts.OUTPUTDIR)
    sys.exit(1)


# Write web page containing all (matched) plots
# Make web pages first so that we can load it locally in
# a browser to view the output before all plots are made

style = '''<style>
      html { font-family: sans-serif; }
      img { border: 0; }
      a { text-decoration: none; font-weight: bold; }
    </style>
    '''

# Include MathJax configuration
script = ''
if not opts.OFFLINE:
    script = '''\
        <script type="text/x-mathjax-config">
        MathJax.Hub.Config({
          tex2jax: {inlineMath: [["$","$"]]}
        });
        </script>
        <script type="text/javascript"
          src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
        </script>
        '''

# A helper function for metadata LaTeX -> HTML conversion

# A timestamp HTML fragment to be used on each page:

timestamp = '<p>Generated at %s</p>\n' % datetime.datetime.now().strftime("%A, %d. %B %Y %I:%M%p")

# Open the top-level index file
index = open(os.path.join(opts.OUTPUTDIR, "index.html"), "w")
reduced_list_of_plots=[]
# Write title
index.write('<html>\n<head>\n<title>%s</title>\n%s</head>\n<body>' %
            (opts.TITLE, style + script))

index.write('<h2>%s</h2>\n\n' % opts.TITLE)
try:
    summary = open("./ANALYSIS/Summary.txt", "r")

    writeSummary(summary,reduced_list_of_plots)

except IOError as e:
    print("No Contur summary file found: Summary info omitted")

index.write("<hr/><h3>List of all the analyses which were tried:</h3>\n")
# Get the make-plots command ready.
mp_cmd = ["make-plots"]
if opts.NUMTHREADS:
    mp_cmd.append("--num-threads=%d" % opts.NUMTHREADS)
if opts.NO_CLEANUP:
    mp_cmd.append("--no-cleanup")
if opts.NO_SUBPROC:
    mp_cmd.append("--no-subproc")
if opts.VECTORFORMAT == "PDF":
    mp_cmd.append("--pdfpng")
elif opts.VECTORFORMAT == "PS":
    mp_cmd.append("--pspng")
if opts.OUTPUT_FONT:
    mp_cmd.append("--font=%s" % opts.OUTPUT_FONT)
if opts.OUTPUT_FONT:
    mp_cmd.append("--font=%s" % opts.OUTPUT_FONT)

datfiledirs = []

# Copy all the dat files from the plots directory into the contur-plot output directory
datfiledirs = glob.glob(opts.OUTPUTDIR+"/*/*")
# loop over all datfile directories
for datfile in datfiledirs:

    plotdir, pool, analysis = datfile.split("/")

    references = []
    summary = htmlify(analysis)
    description, inspireid, spiresid = None, None, None

    if analysis.find("_I") > 0:
        inspireid = analysis[analysis.rfind('_I') + 2:len(analysis)]
    elif analysis.find("_S") > 0:
        spiresid = analysis[analysis.rfind('_S') + 2:len(analysis)]

    ana = rivet.AnalysisLoader.getAnalysis(analysis)
    if ana:
        if ana.summary():
            summary = htmlify("%s (%s) in pool %s" %
                              (ana.summary(), analysis, pool))
        references = ana.references()
        description = htmlify(ana.description())
        spiresid = ana.spiresId()
        if opts.IGNORE_UNVALIDATED and ana.status().upper() != "VALIDATED":
            continue

    # write links for each analysis/pool combination
    index.write(
        '\n<h3><a href="%s/index.html" style="text-decoration:none;">%s</a></h3>\n' % (pool+"/"+analysis, summary))

    reflist = []
    if inspireid:
        reflist.append(
            '<a href="http://inspire-hep.net/record/%s">Inspire</a>' % inspireid)
    elif spiresid:
        # elif ana.spiresId():
        reflist.append(
            '<a href="http://inspire-hep.net/search?p=find+key+%s">Inspire</a>' % spiresid)
    reflist += references
    index.write('<p>%s</p>\n' % " &#124; ".join(reflist))

    if description:
        index.write('<p style="font-size:smaller;">{}</p>\n'.format(description.encode('utf-8')))

    # need the analysis pool here. which probably means we should loop over dat files not the rivet
    anapath = os.path.join(opts.OUTPUTDIR, pool, analysis)

    if not os.path.exists(anapath):
        os.makedirs(anapath)
    anaindex = open(os.path.join(anapath, "index.html"), 'w')
    anaindex.write('<html>\n<head>\n<title>%s in pool %s&ndash; %s</title>\n%s</head>\n<body>\n' %
                   (htmlify(opts.TITLE), analysis, pool, style + script))
    anaindex.write('<h3>%s in pool %s</h3>\n' % (htmlify(analysis), pool))
    anaindex.write('<p><a href="../../index.html">Back to index</a></p>\n')
    anaindex.write('<p>\n  {}\n</p>\n'.format(description.encode('utf-8')))

    adatfiles_tmp = glob.glob(anapath+"/*.dat")
    adatfiles=[]
    for adf in adatfiles_tmp:
      if opts.REDUCEDPLOTS:
        for rp in reduced_list_of_plots:
          if rp in adf: 
            adatfiles+=[adf]
            break
      else:
        adatfiles+=[adf]
    if len(adatfiles) ==0 : continue
    # make the pdf and png files
    mp_this = mp_cmd + adatfiles
    if opts.VERBOSE:
        mp_this.append("--verbose")
        print("Calling make-plots with the following options:")
        print(" ".join(mp_this))
    Popen(mp_this).wait()

    anaindex.write('<div style="float:none; overflow:auto; width:100%">\n')
    if inspireid:
        anaindex.write(
            '<a href="http://inspire-hep.net/record/%s">Inspire</a> <hr/>' % inspireid)
    elif spiresid:
        anaindex.write(
            '<a href="http://inspire-hep.net/search?p=find+key+%s">Inspire</a> <hr/>' % spiresid)

    for datfile in sorted(adatfiles):
        obsname = os.path.basename(datfile).replace(".dat", "")
        pngfile = obsname + ".png"
        vecfile = obsname + "." + opts.VECTORFORMAT.lower()
        srcfile = obsname + ".dat"

        anaindex.write(
            '  <div style="float:left; font-size:smaller; font-weight:bold;">\n')
        anaindex.write('    <a href="#%s-%s">&#9875;</a><a href="%s">&#8984</a> %s:<br/>\n' %
                       (analysis, obsname, srcfile, os.path.splitext(vecfile)[0]))
        anaindex.write('    <a name="%s-%s"><a href="%s">\n' %
                       (analysis, obsname, vecfile))
        anaindex.write('      <img src="%s">\n' % pngfile)
        anaindex.write('    </a></a>\n')
        anaindex.write('  </div>\n')
    anaindex.write('</div>\n')

    anaindex.write(
        '<div style="float:none">%s</body>\n</html></div>\n' % timestamp)
    anaindex.close()

index.write('<br>%s</body>\n</html>' % timestamp)
index.close()


# http://stackoverflow.com/questions/377017/test-if-executable-exists-in-python
def which(program):
    import os

    def is_exe(fpath):
        return os.path.isfile(fpath) and os.access(fpath, os.X_OK)

    fpath, fname = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            path = path.strip('"')
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file

    return None
