Importing the Contur package pulls in the following submodules:

* contur.config - Global configuration options
* contur.data - Database and associated functions parsing data/covariance info from Rivet/YODA
* contur.factories - Main worker classes for contur functionality
* contur.plot - Plotting engine and styling
* contur.scan - Utilities for steering/running creation of MC grids
* contur.util - Misc helper functions

One additional submodule can be manually imported
* contur.run - Defines logic used in python executables

Within contur.factories the main classes (in order of operation are):
* contur.factories.Depot - Handy analysis class for grid creation/spawning other works
* contur.factories.YodaFactory - Wrapper around YODA file format to handle extraction of analysis objects
* contur.factories.HistFactory - Wrapper around YODA analysis object to handle extraction of statistical info
* contur.factories.ConturBucket - Build statistical metric from info fed from HistFactory

