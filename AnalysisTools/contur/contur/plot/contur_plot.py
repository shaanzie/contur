import os
import sys
import warnings

import contur.plot
import contur.util
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from contur.factories.contur_depot import Depot
from cycler import cycler
from matplotlib import rcParams
from matplotlib.gridspec import GridSpec

warnings.filterwarnings("ignore")


# TODO: replace print statements with proper logging a la:
# import logging
# logging.basicConfig(format="%(message)s")

# As we reuse the custom color cycle we will declare it here and add it to the conturPlot class manually, so it can be
# Imported individually elsewhere
# TODO: Fold this all into the conturPlot class when proper label functionality exists
# Pick nice colors from https://matplotlib.org/examples/color/named_colors.html
# TODO: move stuff like this out to an overrideable config file?
CONTURCOLORS = (
    cycler(color=["darkviolet", "darkcyan", "sienna", "firebrick", "navy"]))
# Compatibility alias, not following ALL-CAPS constants std; TODO: remove
conturColors = CONTURCOLORS

# Define a stable set of colours for displaying (dominant) analysis pools, some honed for semantic similarity


def color_tint(col, tintfrac):
    """
    Return a colour as rgb tuple, with a fraction tintfrac of white mixed in
    (or pass a negative tintfrac to mix with black, giving a shade rather than tint
    """
    tintfrac = max(-1.0, min(1.0, tintfrac))
    maincol = mpl.colors.to_rgb(col) if type(col) is str else col
    if tintfrac > 0:
        return (1-tintfrac)*np.array(maincol) + tintfrac*np.array([1., 1., 1.])
    elif tintfrac < 0:
        return (1-abs(tintfrac))*np.array(maincol)
    else:
        return maincol


def getPoolColor(pool, tints=True):
    for poolGroupName in contur.plot.POOLCOLORS:
        # find pool group
        poolGroup = contur.plot.POOLCOLORS[poolGroupName]
        pools = poolGroup["pools"]
        if pool not in pools:
            continue
        color = poolGroup["color"]
        tfrac = 0.1*pow(-1, len(pools)) * pools.index(pool) if tints else 0.0
        return color_tint(color, tfrac)
    print('WARNING: No colour found for pool {}. Using white.'.format(pool))
    return color_tint("White", 1.0)
    # if we didn't find it, make it white
    #	raise KeyError("Pool name could not be found in POOLCOLORS: %s" % pool)


# TODO: Temporary label compatibility, remove in release
axisLabels = {}


class ConturPlotBase(Depot):
    """conturPlotBase extends the conturDepot class to dress things nicely for plotting, it also contains all the steering
    functions the default plotting macro needs

    #TODO Add a config file based setup to stop the command line argument bloat in the plotting macro"""

    def __init__(self, conturDepot, outPath, plotTitle="", savePlotFile=False, omittedPools="", iLevel=3, iSigma=0.75, clLevel=0, style="DRAFT", showcls=False, simplecls=False):

        # extend from a depot class instance
        self.__dict__.update(conturDepot.__dict__)
        self.outputPath = outPath

        self.plotList = []
        self.plotTitle = plotTitle
        self.doPlotPools = True
        self.doSavePlotfile = savePlotFile
        self.omittedPools = omittedPools
        self.iLevel = iLevel
        self.iSigma = iSigma
        self.clLevel = clLevel
        self.showcls = showcls
        self.style = style.upper()
        self.simplecls = simplecls
        self.made_cbar = False

        if self.doSavePlotfile:
            # tell mpl to not worry if we keep a lot of figures open
            rcParams['figure.max_open_warning'] = 100

        self.altData = []
        self.plotObjects = {}

        # Look up table for some convenient default axis labels

        # VLQ stuff
        #axisLabels["xibpw"] = "$\\mathrm{BR}(B \\rightarrow~tW)$"
        #axisLabels["xibph"] = "$\\mathrm{BR}(B \\rightarrow~bH)$"
        axisLabels["xibpw"] = "$\\mathrm{BR}(Q \\rightarrow~qW)$"
        axisLabels["xibph"] = "$\\mathrm{BR}(Q \\rightarrow~qH)$"
        axisLabels["xibpz"] = "$\\mathrm{BR}(B \\rightarrow~bZ)$"
        axisLabels["xitpw"] = "$\\mathrm{BR}(T \\rightarrow~bW)$"
        axisLabels["xitph"] = "$\\mathrm{BR}(T \\rightarrow~tH)$"
        axisLabels["xitpz"] = "$\\mathrm{BR}(T \\rightarrow~tZ)$"
        axisLabels["kappa"] = "$\\kappa$"
        axisLabels["KT"] = "$\\kappa$"
        axisLabels["mtp"] = "$M_{T^\\prime}$ (GeV)"
        #axisLabels["mtp"]   = "$M_Q$ (GeV)"
        axisLabels["mbp"] = "$M_{B^\\prime}$ (GeV)"
        axisLabels["mx"] = "$M_Q$ (GeV)"

        # DM
        axisLabels["mXd"] = "$M_\\mathrm{DM}$ (GeV)"
        axisLabels["mXm"] = "$M_\\mathrm{DM}$ (GeV)"
        axisLabels["gVq"] = "$g_q$"
        axisLabels["gVl"] = "$g_l$"
        axisLabels["gVXd"] = "$g_{DM}$"

        # Zprime
        axisLabels["mY1"] = "$M_{Z^\\prime}$ (GeV)"
        axisLabels["mzp"] = "$M_{Z^\\prime}$ (GeV)"

        # B-L
        axisLabels["g1p"] = "$g_1^{\\prime}$"
        axisLabels["sa"] = "$\\sin\\alpha$"
        #axisLabels["mh2"] = "$M_{h_2}$ (GeV)"

        # TFHM
        axisLabels["tsb"] = "$\\theta_{sb}$"

        # LQ
        axisLabels["mlq"] = "$M_{LQ}$ (GeV)"

        # Heavy Neutrinos
        axisLabels["VeN1"] = "$V_{e_\\nu}$"
        axisLabels["MN1"] = "$M_{\\nu_H}$ (GeV)"

        # 2HDM be careful, these definitions may change between Ken Lane's and everyone else's conventions.
        axisLabels["mh3"] = "$M_A$ (GeV)"
        axisLabels["mh2"] = "$M_{H}$ (GeV)"
        axisLabels["mhc"] = "$M_{H^\pm}$ (GeV)"
        axisLabels["tanbeta"] = "$\\tan\\beta$"
        axisLabels["sinbma"] = "$\\sin(\\beta-\\alpha)$"
        axisLabels["cosbma"] = "$\\cos(\\beta-\\alpha)$"
        # 2HDM+a
        axisLabels["mh4"] = "$M_a$ (GeV)"
        axisLabels["sinp"] = "$\\sin\\theta$"

        # ALPS
        axisLabels["malp"] = "$M_{ALP}$ (GeV)"
        axisLabels["caa"] = "$c_{\\gamma\\gamma}/\\Lambda$ (TeV$^{-1}$)"
        axisLabels["cah"] = "$c_{ah}/\\Lambda$ (TeV$^{-1}$)"
        axisLabels["gpl"] = "$c_{ee}/\\Lambda$ (TeV$^{-1}$)"

        # general light scalar (mphi see below)
        axisLabels["fscale"] = "$\\Lambda$ (GeV)"

        # DE
        axisLabels["c1"] = "$C_1$"
        axisLabels["c2"] = "$C_2$"
        axisLabels["mphi"] = "$M_\\phi$ (GeV)"
        axisLabels["mscale"] = "$M_\\mathrm{SCALE}$ (GeV)"

        # neutrino EFT
        axisLabels["mn1"] = "$m_N$ (GeV)"
        axisLabels["lambda"] = "$\\Lambda$ (GeV)"
        axisLabels["clnh"] = "$\\alpha_{LNH}$"
        axisLabels["cnnh"] = "$\\alpha_{NNH}$"
        axisLabels["cna"] = "$\\alpha_{NA}$"

        # SUSY/SLHA
        axisLabels["1000022"] = "$M(\\tilde{\\chi}_1^0)$ (GeV)"
        axisLabels["1000023"] = "$M(\\tilde{\\chi}_2^0)$ (GeV)"
        axisLabels["1000024"] = "$M(\\tilde{\\chi}_1^+)$ (GeV)"
        axisLabels["1000025"] = "$M(\\tilde{\\chi}_3^0)$ (GeV)"
        axisLabels["1000035"] = "$M(\\tilde{\\chi}_4^0)$ (GeV)"

    def dumpPlotObjects(self):
        """Small function to dump a pickle of the plot objects if requested, this is to allow any mpl image manipulation
        without using the contur package at all
        """
        path_out = os.path.join(self.outputPath, 'contur.plot')
        import pickle
        with open(path_out, 'w') as f:
            pickle.dump(self.plotObjects, f)
        print("Writing output plot dict to:", path_out)

    def buildAxesfromGrid(self, xarg, yarg, logX=False, logY=False, xlabel=None, ylabel=None):
        """Function to build the axes out of the underlying map, creates an AxesHolder instance to store the info and pass
        nicely to the plotting engine

        #TODO Refactor how we store the grids in general, should just reuse the initial scanning functions to build the space OTF"""

        self.checkArgs(xarg, yarg)
        self.buildGrid(xarg, yarg)

        if xlabel:
            xlabel = xlabel
        elif xarg in axisLabels:
            xlabel = axisLabels[xarg]
        else:
            xlabel = xarg

        if ylabel:
            ylabel = ylabel
        elif yarg in axisLabels:
            ylabel = axisLabels[yarg]
        else:
            ylabel = yarg

        # The axes are currently held under the depot, TODO move axes from conturDepot to conturPlotBase
        self.build_axes()

        axHolder = AxesHolder(xAxis=self.map_axis[xarg], xAxisMesh=self.plot_axis[xarg], xLabel=xlabel, xLog=logX,
                              yAxis=self.map_axis[yarg],
                              yAxisMesh=self.plot_axis[yarg], yLabel=ylabel, yLog=logY, title=self.plotTitle)
        # First the combined
        plotBase = conturPlot(saveAxes=self.doSavePlotfile, plotTitle=self.plotTitle,
                              iLevel=self.iLevel, iSigma=self.iSigma, style=self.style, showcls=self.showcls)
        contur.util.mkoutdir(self.outputPath)
        print("Starting plotting engine, outputs written to %s" %
              self.outputPath)
        print("plot combined exclusion limit grid")
        plotBase.addGrid(self.conturGrid, "combined",
                         self.outputPath, axHolder)
        if self.altData:
            plotBase.addAltDataGrids(self.conturGridAltData)

        # Plot the mesh with limit-contour overlays
        plotBase.plotMeshOverlay()
        # Plot the separated limit contour and mesh plots, on one figure and separately
        plotBase.plotHybrid()
        plotBase.plotMesh(make_cbar=True)
        plotBase.plotLevels()

        # Now for pools with highest CLs
        for level in range(self.clLevel):
            print("plot dominant pools level %i (%i/%i)" %
                  (level, level+1, self.clLevel))

            # Simple dpool plot
            levelName = "dominantPools{:d}".format(level)
            plotBase = conturPlot(saveAxes=self.doSavePlotfile, iLevel=self.iLevel,
                                  iSigma=self.iSigma, style=self.style, showcls=self.showcls)
            plotBase.addGrid(self.conturGrid, levelName,
                             self.outputPath, axHolder)
            if self.altData:
                plotBase.addAltDataGrids(self.conturGridAltData)
            plotBase.plotPoolNames(self, level)
            self.plotObjects[levelName] = plotBase.figs

            # Full CLs-info dpool plots
            if not self.simplecls:
                levelName += "CLs"
                plotBase = conturPlot(saveAxes=self.doSavePlotfile, iLevel=self.iLevel,
                                      iSigma=self.iSigma, style=self.style, showcls=self.showcls)
                plotBase.addGrid(self.conturGrid, levelName,
                                 self.outputPath, axHolder)
                plotBase.plotPoolCLs(self, level)
                self.plotObjects[levelName] = plotBase.figs

        # Save the plot data for later cosmetic tweaking
        if self.doSavePlotfile:
            self.plotObjects["combined"] = plotBase.figs

        # Now the individual pools' plots
        if self.doPlotPools:
            outpath = os.path.join(self.outputPath, "pools")
            contur.util.mkoutdir(outpath)

            print("Requested plotting of individual analysis pools, found %s pools to plot" % len(
                self.conturGridPools.keys()))

            for idx, (title, grid) in enumerate(self.conturGridPools.items()):
                print("plot %s (%d/%d done)" %
                      (title, idx+1, len(self.conturGridPools.keys())))
                plotBase = conturPlot(saveAxes=self.doSavePlotfile, iLevel=self.iLevel,
                                      iSigma=self.iSigma, style=self.style, showcls=self.showcls)
                plotBase.addGrid(grid, title, outpath, axHolder)
                plotBase.plotLevels()
                plotBase.plotMesh(make_cbar=False)
                if self.doSavePlotfile:
                    self.plotObjects[title] = plotBase.figs

    def setOutputPath(self, outputpath):
        """Convenience switch to set the output path name for the PlotBase instance"""
        self.outputPath = outputpath

    def checkArgs(self, xarg, yarg):
        """Function to call to check the requested arguments of what to plot are compatible with what is in the map"""
        # for now lets just check against the first point in the list, this should be properly declared from the input file
        try:
            if not all([x in self.inbox[0].paramPoint.keys() for x in (xarg, yarg)]):
                raise ValueError(
                    "Arguments for plotting do not match the available parameters in this map, ensure the parameters are from: %s" % (
                        self.inbox[0].paramPoint.keys()))
        except IndexError as e:
            print(
                'Exception raised: {}. Is it possible this is an empty map file?'.format(e))
            sys.exit(1)

    def addData(self, DataFunctions):
        """Switch to provide the alt data function file to the PlotBase instance"""
        self.altData = DataFunctions

    def buildAltDataGrid(self, xarg, yarg):
        """Build from a grid alternative style"""

        # assume the keys for the parameters are the same for all points and grab from first point
        paramkeys = self.inbox[0].paramPoint.keys()

        self.conturGridAltData = altDataHolder()
        for k, v in self.altData:
            # try:
            store = v(paramkeys)
            # print(store[0])
            if store[0] is not None:
             # except:
               #  raise KeyError("Could not parse paramDict in function %s, make sure only requesting values in: %s" % (
                #    k, paramkeys))
                xaxis = list(np.unique([i[xarg] for i in store[0]]))
                yaxis = list(np.unique([i[yarg] for i in store[0]]))
                self.conturGridAltData.axes.append(AxesHolder(
                    xaxis, 0, xarg, 0, yaxis, 0, yarg, 0, self.plotTitle))
                self.conturGridAltData.label.append(k)
                self.conturGridAltData.grid.append(
                    np.zeros((len(xaxis), len(yaxis))))
            #_temp = {k: np.zeros((len(xaxis), len(yaxis))) for k, v in [t for t in self.altData]}
            #self.conturGridAltData = collections.OrderedDict(sorted(_temp.items(), key=lambda v: (v[0].upper(), v[0].islower())))
                for p, v in zip(store[0], store[1]):
                    xpos = xaxis.index(p[xarg])
                    ypos = yaxis.index(p[yarg])
                    self.conturGridAltData.grid[self.conturGridAltData.label.index(
                        k)][xpos][ypos] = v
                print("Loaded data grid", k)

    def buildSpecialGrid(self, xarg, yarg):
        """buildSpecialGrid allows us to build an empty grid from the sampled points dictionary, this is used for adding custom
        functions to evaluate on the grid. For example see the log-read snippets prepared for the BL paper"""
        # When we are just returning a number loaded with the special input function (e.g. log parsing), the grids have simpler scope
        xaxis = self.map_axis[xarg]
        yaxis = self.map_axis[yarg]
        self.conturGrid = np.zeros((len(xaxis), len(yaxis)))
        for point in self.inbox:
            xpos = list(xaxis).index(float(point.paramPoint[xarg]))
            ypos = list(yaxis).index(float(point.paramPoint[yarg]))
            self.conturGrid[xpos][ypos] = point.yodaFactory

    def getPoolNameFromID(self, currID):
        return list(self.conturGridPools.keys())[currID]

    def buildGrid(self, xarg, yarg):
        """Build the grid in the style mpl needs to make it easy to plot, converts the unordered dictionary of paramPoints into
        a structured numpy array

        #TODO revive ND capabilities, might need a total overhaul of how we do this"""
        # for now we will just scope all the grids we need in the projection
        # fix signs if necessary
        xaxis = self.map_axis[xarg]
        yaxis = self.map_axis[yarg]

        self.conturGrid = np.zeros((len(xaxis), len(yaxis)))
        # again we will just scope the whole grid off the first entry, this should be encoded as a master record in the depot
        self.conturGridPools = {key: np.zeros((len(xaxis), len(yaxis))) for key in
                                [p.pools for p in self.inbox[0].yodaFactory.sortedBuckets]}
        # It would be nicer to reuse the sampled grid but that isn't high enough resoltion
        if self.altData:
            self.buildAltDataGrid(xarg, yarg)

        self.poolIDs = np.full((len(xaxis), len(yaxis), len(
            self.conturGridPools.keys())), -1, dtype=int)
        self.poolCLs = np.full((len(xaxis), len(yaxis), len(
            self.conturGridPools.keys())), -1, dtype=float)

        for point in self.inbox:
            xpos = list(xaxis).index(float(point.paramPoint[xarg]))
            ypos = list(yaxis).index(float(point.paramPoint[yarg]))
            if self.omittedPools:
                point.yodaFactory.sortBuckets(self.omittedPools)

            self.conturGrid[xpos][ypos] = point.yodaFactory.conturPoint.CLs

            for zpos, p in enumerate(point.yodaFactory.sortedBuckets):
                try:
                    self.conturGridPools[p.pools][xpos][ypos] = p.CLs
                    self.poolIDs[xpos][ypos][zpos] = list(self.conturGridPools.keys()).index(
                        p.pools)
                    self.poolCLs[xpos][ypos][zpos] = p.CLs * 100  # in %
                except KeyError:
                    KeyError("Could not find pool %s for point %s, grid might be malformed" % (
                        p, point.paramPoint))

        if not self.clLevel > 0:
            return  # nothing to do anymore if clLevel plots are not printed

        # sort by CLs: get sorting index from poolCLs, z-axis; reverse order by "-"
        index = np.argsort(-self.poolCLs, axis=2)

        # sort poolGrids
        try:
            self.poolIDs = np.take_along_axis(self.poolIDs, index, axis=2)
            self.poolCLs = np.take_along_axis(self.poolCLs, index, axis=2)
        except Exception as e:
            print(e)
            print(
                "The problem may be you have numpy older than 1.15.0. Upgrade, or set --num-dpools 0")
            sys.exit(1)

        # remove unneccessary entries
        self.poolIDs = self.poolIDs[:, :, :self.clLevel]
        self.poolCLs = self.poolCLs[:, :, :self.clLevel]

        self.poolNames = [[]]
        numAvailLevels = self.poolCLs.shape[2]
        if self.clLevel > numAvailLevels:
            print("The number of requested levels of dominant pools (%i) is larger than the number of available dominant pools (%i)." % (
                self.clLevel, numAvailLevels))
            print(
                "Setting number of requested levels to number of available pools (%i)." % numAvailLevels)
            self.clLevel = numAvailLevels
        for level in range(self.clLevel):
            # make lists out of 2D arrays
            listCLs = self.poolCLs[:, :, level].flatten()
            listIDs = self.poolIDs[:, :, level].flatten()

            # sort IDs by CLs
            listIDs = np.take_along_axis(listIDs, np.argsort(-listCLs), axis=0)

            # remove duplicates
            distinctListIDs = []
            for currID in listIDs:
                if not currID in distinctListIDs:
                    distinctListIDs.append(currID)

            # find up to max_pools-1/ max_pools highest-CLs pools
            usefulKeys = []
            max_pools = 20
            # we have exactly max_pools or less contributing pools, which our colormap can support
            if len(distinctListIDs) <= max_pools:
                usefulKeys = distinctListIDs
            else:  # select only up to max_pools-1 leading pools so we can add one pool "others"
                usefulKeys = distinctListIDs[:max_pools-1]

            # create shortlist of pool names
            self.poolNames.append([])
            for entry in usefulKeys:
                self.poolNames[level].append(self.getPoolNameFromID(entry))

            # sort poolKeys and poolNames
            sort_by_hue = True
            if sort_by_hue:  # get sort index for hue sort
                import matplotlib.colors as mcolors
                poolhsvs = []
                for poolName in self.poolNames[level]:
                    poolhsvs.append(tuple(mcolors.rgb_to_hsv(mcolors.to_rgb(
                        getPoolColor(poolName)))))  # < TODO: tint control
                sort_index = np.argsort(poolhsvs, axis=0)[:, 0]
            else:  # get sort index for alphabetical sort
                poolInfo = np.array(
                    [np.array([x.split("_")[0], x.split("_")[2]]) for x in self.poolNames[level]])
                poolEnergies = np.array([int(x.split("_")[1])
                                         for x in self.poolNames[level]])
                sort_index = np.lexsort((poolInfo[:, 1], poolEnergies, poolInfo[:, 0]))[
                    ::-1]  # get sort index, [::-1] for reverse order

            self.poolNames[level] = list(np.take_along_axis(
                np.array(self.poolNames[level]), sort_index, axis=0))  # sort by given index
            usefulKeys = list(np.take_along_axis(
                np.array(usefulKeys), sort_index, axis=0))  # sort by given index

            # insert dummy for all other pools if not <=max_pools contributing pools
            if not len(distinctListIDs) <= max_pools:
                # need to add to usefulKeys as well so that indices match
                usefulKeys.insert(0, -1)
                self.poolNames[level].insert(0, "other")

            # change IDs in poolIDs to shorter ID list
            otherPools = []
            # loop over all entries of 3D matrix, allowing for modifications
            with np.nditer(self.poolIDs[:, :, level], op_flags=['readwrite']) as it:
                for entry in it:
                    if entry >= 0:
                        try:
                            newID = usefulKeys.index(entry)
                        except ValueError:  # pool is not important enough for shorter index; list as "other"
                            poolName = self.getPoolNameFromID(entry)
                            if not poolName in otherPools:
                                otherPools.append(poolName)
                            newID = -1
                        entry[...] = newID
            # print "level:", level
            # print "other pools:", otherPools, "\n"


class AxesHolder(object):
    """Data structure to keep things legible in the code, holds the Axes scoped from the map file and information about
    how we visualise it. Just used for book keeping
    """

    def __init__(self, xAxis, xAxisMesh, xLabel, xLog, yAxis, yAxisMesh, yLabel, yLog, title):
        self.xAxis = xAxis
        self.xAxisMesh = xAxisMesh
        self.xLabel = xLabel
        self.xLog = xLog
        self.yAxis = yAxis
        self.yAxisMesh = yAxisMesh
        self.yLabel = yLabel
        self.yLog = yLog
        self.title = title


class altDataHolder(object):
    """Data structure to keep things legible in the code, book keep for alternative theory grids as each may have different sampled values
    """

    def __init__(self):
        self.label = []
        self.grid = []
        self.axes = []


class conturPlot(object):
    """conturPlot is the engine that interacts with the matplotlib.pyplot plotting library"""

    def __init__(self, saveAxes=False, plotTitle="", iLevel=3, iSigma=0.75, style="DRAFT", showcls=False, simplecls=False):
        # Initialise the basic single plot style
        self.loadStyleDefaults()
        self.altDataGrids = {}
        self.figs = []
        self.saveAxes = saveAxes
        self.plotTitle = plotTitle
        self.iLevel = iLevel
        self.iSigma = iSigma
        self.style = style.upper()
        self.showcls = showcls
        self.simplecls = simplecls
        self.cmap = plt.cm.viridis

    def addLimits(self, ax, colorCycle, filled=True):
        "Add the overlaid extra limit contours"
        for l, g, a in self.altDataGrids:
            # Can't see a quick way for contourf to use the default prop_cycle, we will still use that but have a small hack
            c = next(colorCycle)["color"]
            if filled:
                ax.contourf(a.xAxis, a.yAxis, g.T, colors=c, levels=[
                            0.0, 10.0], alpha=0.3)  # , snap=True)
            ax.contour(a.xAxis, a.yAxis, g.T, colors=c, levels=[0.0, 10.0])

        # TODO Is it possible to make this nicer? Best solution is either a config file or save MPL objects in pickle for later manipulation
        # =======================

        # TODO The contur/plot directory has a file LabelMaker to define labels to add here, in future we could replicate the theory function file input to give a label input from file?
        # LabelMaker.BLCaseDE(self.axes[0])
        # LabelMaker.BLCaseA(self.axes[0])
        # LabelMaker.BLCaseB(self.axes[0])
        # LabelMaker.BLCaseC(self.axes[0])
        # LabelMaker.DM_LF(self.axes[0])

    def plotHybrid(self):
        """Build the default contur output for combined limit, a hybrid plot showing both a colormesh of the underlying
        exclusion and the derived 1 and 2 sigma confidence intervals from this space"""

        self.fig, self.axes = plt.subplots(nrows=1, ncols=3, figsize=self.fig_dims_hybrid,
                                           gridspec_kw={"width_ratios": [1, 1, 0.08]})
        self.axes[1].set_title(
            label=r"\textsc{Contur}" + str(self.plotTitle), loc="right")  # \textsc{Contur}

        im0 = self.axes[1].pcolormesh(self.xAxisMesh, self.yAxisMesh, self.grid.T, cmap=self.cmap, vmin=0, vmax=1,
                                      snap=True)

        path_out = os.path.join(self.destination, self.label + "Hybrid")
        if self.xLog:
            self.axes[1].set_xscale("log", nonposx='clip')
        if self.yLog:
            self.axes[1].set_yscale("log", nonposy='clip')
        # self.axes[0].set_ylabel(self.yLabel)
        self.axes[1].set_xlabel(self.xLabel)

        self.interpolateGrid(self.iLevel, self.iSigma)

        self.axes[0].contourf(self.xaxisZoom, self.yaxisZoom, self.gridZoom.T, cmap=self.cmap, levels=[0.68, 0.95, 10],
                              vmin=0.0, vmax=1.0)  # , alpha=0.6)  # , snap=True)
        self.axes[0].contour(self.xaxisZoom, self.yaxisZoom, self.gridZoom.T, colors="black", levels=[0.68, 0.95, 10],
                             vmin=0.0, vmax=1.0)

        # add theory/previous experiment limits
        colorCycle = iter(CONTURCOLORS())
        self.addLimits(self.axes[0], colorCycle)

        if self.xLog:
            self.axes[0].set_xscale("log", nonposx='clip')
        if self.yLog:
            self.axes[0].set_yscale("log", nonposy='clip')
        self.axes[0].set_ylabel(self.yLabel)
        self.axes[0].set_xlabel(self.xLabel)

        self.axes[0].set_ylim(top=max(self.yaxisZoom),
                              bottom=min(self.yaxisZoom))
        self.axes[0].set_xlim(right=max(self.xaxisZoom),
                              left=min(self.xaxisZoom))
        self.axes[1].set_ylim(top=max(self.yaxisZoom),
                              bottom=min(self.yaxisZoom))
        self.axes[1].set_xlim(right=max(self.xaxisZoom),
                              left=min(self.xaxisZoom))

        # self.axes[0].set_ymin(min(self.yaxisZoom))
        # self.axes[0].set_ymax(max(self.yaxisZoom))

        # self.axes[1].get_shared_y_axes().join(self.axes[0], self.axes[1])
        # the shares axes are being a bugger
        self.axes[1].set_yticklabels([])
        # self.axes[1].set_yticks(self.axes[0].get_yticks())
        cbar = self.fig.colorbar(im0, cax=self.axes[2])
        cbar.set_label(r"CL$_{s}$")

        if self.showcls:
            self.induceCLsGrid(self.axes[1], self.grid)

        try:
            self.fig.tight_layout(pad=0.32)
            self.fig.savefig(path_out + ".pdf", format="pdf")
            if not self.saveAxes:
                plt.close(self.fig)
        except Exception as e:
            print("Failed to make combinedHybrid plot. This may be due to the interpolation step. Try a different iLevel?")
            print(e)
            sys.exit()

    def plotLevels(self):
        """Make an individual levels plot, currently just used for compatibility to show the individual pools
        TODO Derive these from the main hybrid plot"""
        # make a styled blank canvas
        self.makeCanvas()

        # interpolate the meshgrid to make things smoother for a levels plot
        self.interpolateGrid(self.iLevel, self.iSigma)
        self.ax0.contourf(self.xaxisZoom, self.yaxisZoom, self.gridZoom.T, cmap=self.cmap, levels=[0.68, 0.95, 10],
                          vmin=0.0, vmax=1.0)  # , alpha=0.6)  # , snap=True)
        self.ax0.contour(self.xaxisZoom, self.yaxisZoom, self.gridZoom.T, colors="black", levels=[0.68, 0.95, 10],
                         vmin=0.0, vmax=1.0)

        path_out = os.path.join(self.destination, self.label + "Levels")
        if self.xLog:
            self.ax0.set_xscale("log", nonposx='clip')
        if self.yLog:
            self.ax0.set_yscale("log", nonposy='clip')
        self.ax0.set_ylim(top=max(self.yaxisZoom), bottom=min(self.yaxisZoom))
        self.ax0.set_xlim(right=max(self.xaxisZoom), left=min(self.xaxisZoom))
        self.ax0.set_ylim(top=max(self.yaxisZoom), bottom=min(self.yaxisZoom))
        self.ax0.set_xlim(right=max(self.xaxisZoom), left=min(self.xaxisZoom))

        self.ax0.set_ylabel(self.yLabel)
        self.ax0.set_xlabel(self.xLabel)

        self.figs.append(self.fig)
        self.fig.tight_layout(pad=0.1)
        self.fig.savefig(path_out + ".pdf", format="pdf")
        if not self.saveAxes:
            plt.close(self.fig)

    def plotMesh(self, make_cbar):
        """Make an individual colormesh plot, currently just used for compatibility to show the individual pools
        TODO Derive these from the main hybrid plot"""
        # make a styled blank canvas
        self.makeCanvas()
        self.ax0.pcolormesh(self.xAxisMesh, self.yAxisMesh,
                            self.grid.T, cmap=self.cmap, vmin=0, vmax=1, snap=True)

        path_out = os.path.join(self.destination, self.label + "Mesh")
        if self.xLog:
            self.ax0.set_xscale("log", nonposx='clip')
        if self.yLog:
            self.ax0.set_yscale("log", nonposy='clip')
        self.ax0.set_ylim(top=max(self.yaxisZoom), bottom=min(self.yaxisZoom))
        self.ax0.set_xlim(right=max(self.xaxisZoom), left=min(self.xaxisZoom))
        self.ax0.set_ylim(top=max(self.yaxisZoom), bottom=min(self.yaxisZoom))
        self.ax0.set_xlim(right=max(self.xaxisZoom), left=min(self.xaxisZoom))
        self.ax0.set_ylabel(self.yLabel)
        self.ax0.set_xlabel(self.xLabel)

        if self.showcls:
            self.induceCLsGrid(self.ax0, self.grid)

        self.figs.append(self.fig)
        self.fig.tight_layout(pad=0.1)
        self.fig.savefig(path_out + ".pdf", format="pdf")
        if not self.saveAxes:
            plt.close(self.fig)

            # make (one!) separate fig colorbar
        if make_cbar:
            self.fig_cbar = plt.figure(figsize=self.fig_dims_cbar)  # _cbar)
            self.axcbar = self.fig_cbar.add_subplot(1, 1, 1)
            norm = mpl.colors.Normalize(vmin=0, vmax=1)
            cb1 = mpl.colorbar.ColorbarBase(
                self.axcbar, cmap=self.cmap, norm=norm, orientation="vertical")  # ,orientation="vertical")
            cb1.set_label(r"CL$_{s}$")
            self.fig_cbar.tight_layout(pad=0.1)
            self.fig_cbar.savefig(path_out + "cbar.pdf", format="pdf")
            if not self.saveAxes:
                plt.close(self.fig)
                plt.close(self.fig_cbar)

    def plotMeshOverlay(self, make_cbar=False):
        """Make an individual colormesh plot with overlaid limit contours"""
        self.makeCanvas()

        # draw the mesh
        self.ax0.pcolormesh(self.xAxisMesh, self.yAxisMesh,
                            self.grid.T, cmap=self.cmap, vmin=0, vmax=1, snap=True)
        if self.xLog:
            self.ax0.set_xscale("log", nonposx='clip')
        if self.yLog:
            self.ax0.set_yscale("log", nonposy='clip')
        self.ax0.set_ylim(top=max(self.yaxisZoom), bottom=min(self.yaxisZoom))
        self.ax0.set_xlim(right=max(self.xaxisZoom), left=min(self.xaxisZoom))
        self.ax0.set_ylim(top=max(self.yaxisZoom), bottom=min(self.yaxisZoom))
        self.ax0.set_xlim(right=max(self.xaxisZoom), left=min(self.xaxisZoom))
        self.ax0.set_ylabel(self.yLabel)
        self.ax0.set_xlabel(self.xLabel)

        # add theory/previous experiment limits
        colorCycle = iter(CONTURCOLORS())
        self.addLimits(self.ax0, colorCycle)

        # interpolate the meshgrid to make things smoother for a levels plot
        self.interpolateGrid(self.iLevel, self.iSigma)
        self.ax0.contour(self.xaxisZoom, self.yaxisZoom, self.gridZoom.T, colors="white", levels=[
                         0.68, 0.95], linestyles=["dashed", "solid"], vmin=0.0, vmax=1.0)

        if self.showcls:
            self.induceCLsGrid(self.ax0, self.grid)

        self.figs.append(self.fig)
        self.fig.tight_layout(pad=0.1)

        path_out = os.path.join(self.destination, self.label + "Overlay")
        self.fig.savefig(path_out + ".pdf", format="pdf")
        if not self.saveAxes:
            plt.close(self.fig)

        # make a separate fig colorbar
        if make_cbar:
            self.fig_cbar = plt.figure(figsize=self.fig_dims_cbar)  # _cbar)
            self.axcbar = self.fig_cbar.add_subplot(1, 1, 1)
            norm = mpl.colors.Normalize(vmin=0, vmax=1)
            cb1 = mpl.colorbar.ColorbarBase(
                self.axcbar, cmap=self.cmap, norm=norm, orientation="vertical")  # ,orientation="vertical")
            cb1.set_label(r"CL$_{s}$")
            self.fig_cbar.tight_layout(pad=0.1)
            self.fig_cbar.savefig(path_out + "cbar.pdf", format="pdf")
            if not self.saveAxes:
                plt.close(self.fig)
                plt.close(self.fig_cbar)

    # show the bin contents as text

    def induceCLsGrid(self, axis, grid, inPercent=False):
        for i in range(len(self.xAxis)):
            for j in range(len(self.yAxis)):
                z = "%.2f" % grid[i, j]
                if inPercent:
                    z = "%.1f" % grid[i, j]
                axis.text(self.xAxis[i], self.yAxis[j], z, color="w",
                          ha="center", va="center", fontsize="4")

    def prepareAxis(self, axis):
        if self.xLog:
            axis.set_xscale("log", nonposx='clip')
        if self.yLog:
            axis.set_yscale("log", nonposy='clip')
        axis.set_ylim(top=max(self.yaxisZoom), bottom=min(self.yaxisZoom))
        axis.set_xlim(right=max(self.xaxisZoom), left=min(self.xaxisZoom))
        axis.set_ylabel(self.yLabel)
        axis.set_xlabel(self.xLabel)

    # plot a mesh of the grid with CLs values

    def plotCLs(self, gridSpecs, grid, title, extend='neither'):
        axis = self.fig.add_subplot(gridSpecs[0])
        axisCbar = self.fig.add_subplot(gridSpecs[1])
        axis.set_title(title)

        vmin = 0
        cmap = self.cmap
        if extend == 'min':
            vmin = -1
            newcolors = self.cmap(np.linspace(0, 1, 255))
            np.insert(newcolors, 0, [0, 0, 0, 1])
            newcolors[0, :] = np.array([0, 0, 0, 1])
            cmap = mpl.colors.ListedColormap(newcolors)

        plot = axis.pcolormesh(self.xAxisMesh, self.yAxisMesh,
                               grid.T, cmap=cmap, vmin=vmin, vmax=100, snap=True)
        self.prepareAxis(axis)

        cbarTotal = self.fig.colorbar(plot, cax=axisCbar, extend=extend)
        cbarTotal.set_label(r"CL$_{s}$")

        if self.showcls:
            self.induceCLsGrid(axis, grid, inPercent=True)

    def plotPoolCLs(self, cpb, level):
        """Make a 2D plot of the pools' CLs"""
        diff = 0.07
        widthRatios = [1, 0.04]
        hspace = 0.4
        top = 0.95
        gsL = GridSpec(2, 2, width_ratios=widthRatios,
                       right=0.5-diff, hspace=hspace, top=top)
        gsR = GridSpec(2, 2, width_ratios=widthRatios,
                       left=0.5+diff, hspace=hspace, top=top)
        self.fig = plt.figure(figsize=self.fig_dims_cls)
        axisLeading = self.fig.add_subplot(gsR[2])
        axisLeadingCbar = self.fig.add_subplot(gsR[3])

        # ==================================================================
        # CLs meshs
        # ==================================================================
        # total CLs
        gridTotal = self.grid*100
        self.plotCLs([gsL[0], gsL[1]], gridTotal, "total CLs")

        # leading CLs
        self.plotCLs([gsR[0], gsR[1]], cpb.poolCLs[:, :, level],
                     "(sub)$^"+str(level)+"$ leading CLs")

        # diff CLs
        self.plotCLs([gsL[2], gsL[3]], gridTotal - cpb.poolCLs[:, :, level],
                     "total CLs - (sub)$^"+str(level)+"$ leading CLs", 'min')

        # ==================================================================
        # leading CLs: pool names
        # ==================================================================
        axisLeading.set_title("(sub)$^"+str(level)+"$ leading CLs: pool names")
        # TODO: can we somehow get a more semantically ordered pool list, e.g. using the POOLCOLORS dict order?
        nColors = len(cpb.poolNames[level])
        colorCycle = iter(CONTURCOLORS())  # < this thing is horrible to use...

        #usetints = (self.style == "FINAL")
        # always use tints for this detailed plot.
        usetints = True
        #plotHighest = axisLeading.pcolormesh(self.xAxisMesh, self.yAxisMesh, cpb.poolIDs[:,:,level].T, snap=True, cmap=plt.get_cmap("tab20", nColors), vmin=0, vmax=nColors)
        poolcmap = plt.matplotlib.colors.ListedColormap(
            [getPoolColor(pool, usetints) for pool in cpb.poolNames[level]])
        plotHighest = axisLeading.pcolormesh(
            self.xAxisMesh, self.yAxisMesh, cpb.poolIDs[:, :, level].T, snap=True, cmap=poolcmap, vmin=0, vmax=nColors)
        self.prepareAxis(axisLeading)

        if self.showcls:
            self.induceCLsGrid(
                axisLeading, cpb.poolCLs[:, :, level], inPercent=True)

        bounds = np.linspace(0, nColors, nColors+1)
        ticks = [x+0.5 for x in bounds]
        labels = []
        for pool in cpb.poolNames[level]:
            # have to escape underscores
            labels.append(pool.replace("_", "\_"))
        cbar = self.fig.colorbar(
            plotHighest, cax=axisLeadingCbar, boundaries=bounds, ticks=ticks)
        cbar.ax.set_yticklabels(labels)
        cbar.ax.tick_params(labelsize=4)

        # ==================================================================
        # clean up
        # ==================================================================
        self.figs.append(self.fig)
        path_out = os.path.join(self.destination, self.label)  # + "PoolCLs")
        self.fig.savefig(path_out + ".pdf", format="pdf")
        if not self.saveAxes:
            plt.close(self.fig)

    def plotPoolNames(self, cpb, level):
        """Make a 2D plot of the dominant pool names and their CLs values"""
        self.makeCanvas()
        path_out = os.path.join(self.destination, self.label)

        # Plot styling
        showtitle = (self.style == "DRAFT")
        showcbar = (self.style == "DRAFT")

        # Painstakingly assemble a nice title
        if showtitle:
            title = "Leading CLs analysis pools"
            if level > 0:
                suffix = "th"
                if level < 3:
                    suffix = "st" if level == 1 else "nd"
                title = "{lev:d}{suff}-subleading-CLs analysis pools".format(
                    lev=level, suff=suffix)
            self.ax0.set_title(title)

        # TODO: can we somehow get a more semantically ordered pool list, e.g. using the POOLCOLORS dict order?
        nColors = len(cpb.poolNames[level])

        usetints = not (self.style == "FINAL")
        # plotHighest = self.ax0.pcolormesh(self.xAxisMesh, self.yAxisMesh, cpb.poolIDs[:,:,level].T, snap=True, cmap=plt.get_cmap("tab20", nColors), vmin=0, vmax=nColors)
        poolcmap = plt.matplotlib.colors.ListedColormap(
            [getPoolColor(pool, usetints) for pool in cpb.poolNames[level]])
        plotHighest = self.ax0.pcolormesh(
            self.xAxisMesh, self.yAxisMesh, cpb.poolIDs[:, :, level].T, snap=True, cmap=poolcmap, vmin=0, vmax=nColors)
        self.prepareAxis(self.ax0)

        # Add theory/previous experiment limits
        colorCycle = iter(CONTURCOLORS())
        # self.addLimits(self.ax0, colorCycle, False)
        for l, g, a in self.altDataGrids:
            c = color_tint(next(colorCycle)["color"], 0.65)
            self.ax0.contour(a.xAxis, a.yAxis, g.T, colors=[
                             c], levels=[0.0, 10.0])

        # Don't show the CLs numbers with the simple plot
        self.interpolateGrid(self.iLevel, self.iSigma)
        self.ax0.contour(self.xaxisZoom, self.yaxisZoom, self.gridZoom.T, colors="white", levels=[
                         0.68, 0.95], linestyles=["dashed", "solid"], vmin=0.0, vmax=1.0)

        # Colour bar
        bounds = np.linspace(0, nColors, nColors+1)
        ticks = [x+0.5 for x in bounds]
        labels = []
        for pool in cpb.poolNames[level]:
            # have to escape underscores
            labels.append(pool.replace("_", "\_"))
        if showcbar:
            # self.fig.savefig(path_out + "nocbar.pdf", format="pdf") #< save before adding the cbar
            cbar = self.fig.colorbar(
                plotHighest, boundaries=bounds, ticks=ticks)
            cbar.ax.set_yticklabels(labels)
            cbar.ax.tick_params(labelsize=4)
        # else:
        #     cbfig, cbax = plt.subplots(figsize=self.fig.get_size_inches()*self.fig.dpi)
        #     cbar = self.fig.colorbar(plotHighest, ax=cbax, boundaries=bounds, ticks=ticks)
        #     cbax.set_yticklabels(labels)
        #     cbax.tick_params(labelsize=4)
        #     cbax.remove()
        #     cbfig.savefig(path_out + "cbar.pdf", format="pdf")
        #     plt.close(cbfig)

        # Tidy the presentation
        self.figs.append(self.fig)
        self.fig.tight_layout(pad=0.1)

        # Save fig and cbar
        self.fig.savefig(path_out + ".pdf", format="pdf")

        # Clean up
        if not self.saveAxes:
            plt.close(self.fig)

    def makeCanvas(self):
        """Convenience function for the individual plots"""
        self.fig = plt.figure(figsize=self.fig_dims)
        self.ax0 = self.fig.add_subplot(1, 1, 1)

    def addGrid(self, numpyGrid, label, dest, axHolder):
        """Main access method to give the plot all the attributes it needs in a numpy format that feeds directly into mpl"""
        self.grid = numpyGrid
        self.label = label
        self.destination = dest
        self.__dict__.update(axHolder.__dict__)

        self.interpolateGrid(self.iLevel)

    def addAltDataGrids(self, altDataGrids):
        """Add the alternative data grids, this is a workaround for now
        TODO: Revisit the implementation of smoothing using scipy's interpolator here"""
        self.altDataGrids = zip(
            altDataGrids.label, altDataGrids.grid, altDataGrids.axes)

    def interpolateGrid(self, level=3, sigma=0.75):
        """Use scipy's interpolators to create smoothed & zoomed versions of the grids and axes"""
        import scipy.ndimage
        self.gridZoom = scipy.ndimage.zoom(self.grid, level)
        self.gridZoom = scipy.ndimage.gaussian_filter(
            self.gridZoom, sigma*level)
        self.xaxisZoom = scipy.ndimage.zoom(self.xAxis, level)
        self.yaxisZoom = scipy.ndimage.zoom(self.yAxis, level)

    def loadStyleDefaults(self):
        """Some core common styling such as figure dimensions, and rcParams"""
        WIDTH = 454.0
        FACTOR = 1.0 / 2.0
        figwidthpt = WIDTH * FACTOR
        inchesperpt = 1.0 / 72.27
        golden_ratio = (np.sqrt(5) - 1.0) / 2.0

        figwidthin = figwidthpt * inchesperpt  # figure width in inches
        figheightin = figwidthin * golden_ratio + 0.6  # figure height in inches
        self.fig_dims = [figwidthin, figheightin]  # fig dims as a list
        self.fig_dims_hybrid = [figwidthin * 1.8, figheightin]
        self.fig_dims_cls = [figwidthin * 3., 2.1*figheightin]
        self.fig_dims_cbar = [figwidthin * 0.2, figheightin]

        document_fontsize = 10
        rcParams['font.family'] = 'serif'
        rcParams['font.serif'] = ['Computer Modern Roman']
        rcParams['font.size'] = document_fontsize
        rcParams['axes.titlesize'] = document_fontsize
        rcParams['axes.labelsize'] = document_fontsize
        rcParams['xtick.labelsize'] = document_fontsize
        rcParams['ytick.labelsize'] = document_fontsize
        rcParams['legend.fontsize'] = document_fontsize
        rcParams['text.usetex'] = True
        rcParams['interactive'] = False
        rcParams['axes.prop_cycle'] = CONTURCOLORS
