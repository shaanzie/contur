"""
Perform various manipulations on an existing contur scan grid or grids, but NOT the actual contur statistical analysis.
"""

import logging
import sys
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter

import contur
from contur.scan.grid_tools import grid_loop


def get_args(argv):
    parser = ArgumentParser(usage=__doc__, formatter_class=ArgumentDefaultsHelpFormatter)

    options = parser.add_argument_group("Control options")

    parser.add_argument('scan_dirs', nargs='*', metavar='scan_dirs',
                        help='scan directories to process.')

    options.add_argument("-m", "--merge", action="store_true", dest="MERGE_GRIDS",
                        default=False, help="merge two or more grids using symbolic links. Excludes other options")

    options.add_argument("-r", "--remove-merged", action="store_true", dest="RM_MERGED",
                        default=False, help="if unmerged yodas exist, unzip them, and remove merged ones")

    options.add_argument("-x", action="append", dest="ANAPATTERNS", default=[],
                          help="create a new grid containing output of only these analyses")

    options.add_argument("-n", "--no-clean", action="store_true", dest="DO_NOT_CLEAN",
                        default=False, help="do not remove unnecessary files.")

    options.add_argument("-a", "--archive", action="store_true", dest="COMPRESS_GRID",
                        default=False, help="remove intermediate and unncessary files, and compress others.")

    options.add_argument("-c", "--check", action="store_true", dest="CHECK_GRID",
                        default=False, help="check whether all grid points have valid yodas")

    options.add_argument("--ca", "--check-all", action="store_true", dest="CHECK_ALL",
                        default=False, help="include grid points without logfiles when checking for yodas")

    options.add_argument("-s", "--submit", action="store_true", dest="RESUB",
                        default=False, help="(re)submit any jobs which are found to have failed.")
    options.add_argument('-B', '--batch', dest="batch_system", default='qsub',
                        type=str, metavar='batch_system',
                        help="Specify which batch system is using, support: qsub or slurm")
    options.add_argument("-q", "--queue", dest="queue", metavar="queue", default="",
                        type=str, help="batch queue.")
    options.add_argument("-f", "--findPoint", action="append", dest="FINDPARAMS", default=[],
                          help="identify points consistent with these parameters and make histograms for them")


    args = parser.parse_args(argv)
    return args


def main(args):

    contur.config.contur_log.setLevel(logging.INFO)

    if len(args.scan_dirs) == 0:
        contur.config.contur_log.critical("No grid directory specified")
        sys.exit(1)

    Clean = True
    if args.DO_NOT_CLEAN:
        contur.config.contur_log.info("Not removing unnecessary files from grid")
        Clean=False

    if args.MERGE_GRIDS:
        print("Merging Grids")
        contur.scan.merge_main(sys.argv[2:])
        sys.exit(0)

    elif args.ANAPATTERNS:
        contur.config.contur_log.info("Extract histograms from {} into a new grid".format(args.ANAPATTERNS))
        if not len(args.scan_dirs)==1:
            contur.config.contur_log.critical("Requires exactly one directory. {} given. ({})".format(len(args.scan_dirs),args.scan_dirs))
            sys.exit(1)

        grid_loop(scan_path=args.scan_dirs[0], patterns=args.ANAPATTERNS, extract=True, clean=Clean)

    elif args.RM_MERGED:
        contur.config.contur_log.info("If unmerged yodas exists, unzipping them and removing merge yodas.")
        grid_loop(scan_path=args.scan_dirs[0], unmerge=True, clean=Clean)

    elif args.COMPRESS_GRID:
        contur.config.contur_log.info("Archiving this directory tree")
        grid_loop(scan_path=args.scan_dirs[0], archive=True)

    elif args.CHECK_GRID or args.CHECK_ALL:
        contur.config.contur_log.info("Checking directory tree")
        if args.CHECK_ALL:
            contur.config.contur_log.info("Also counting jobs without batch logs as failed")
        grid_loop(scan_path=args.scan_dirs[0], check=True, resub=args.RESUB, check_all=args.CHECK_ALL,queue=args.queue)


    elif args.FINDPARAMS:
        # find the specified parameter point.
        yodaFiles = contur.util.findParamPoint(args.scan_dirs,contur.config.tag,args.FINDPARAMS)

    elif Clean:
        grid_loop(scan_path=args.scan_dirs[0], clean=Clean)

    sys.exit(0)
