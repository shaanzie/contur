import os
import sys
import shutil
import subprocess

import contur
import rivet
import yoda
from contur.run.run_batch_submit import gen_submit_command


def grid_loop(scan_path=None,patterns=None,extract=False,clean=True,unmerge=False,archive=False,check=False,check_all=False,resub=False,queue=""):

    if not os.path.isdir(scan_path):
        contur.config.contur_log.critical("Expected a directry containing a contur grid. Terminating.")
        sys.exit(1)
        
    if archive:
        clean=True
        extract=False
        unmerge=False

    if clean:
        contur.config.contur_log.info("Removing unnecessary files from grid")

    if extract:
        new_dir = scan_path+"_new"
        contur.scan.make_directory(new_dir)


    for root, dirs, files in sorted(os.walk(scan_path)):

        got_valid_yoda = False
        got_batch_logs = False
        batch_script = False

        # removing unnecessary files
        if clean:
            contur.scan.delete_files(root)

        run_point_num = os.path.basename(root)


        got_merged_yoda_file = False
        merged_yoda_file = os.path.join(root, contur.config.tag + run_point_num + '.yoda')
        merged_yoda_file_gz = merged_yoda_file+'.gz'
        if not unmerge:
            # gzipping merged yoda file if unzipped file exists
            if os.path.exists(merged_yoda_file):
                if os.path.exists(merged_yoda_file_gz):
                    os.remove(merged_yoda_file_gz)
                command = 'gzip {}'.format(merged_yoda_file)
                contur.config.contur_log.debug(command)
                os.system(command)
        got_merged_yoda_file = contur.util.isValidYodafile(merged_yoda_file_gz)

        if got_merged_yoda_file:
            param_file_path = os.path.join(root, contur.config.paramfile)
            params = contur.util.read_param_point(param_file_path)
            contur.config.contur_log.debug('\nFound valid yoda file ' + merged_yoda_file_gz.strip('./'))
            sample_str = 'Sampled at:'
            for param, val in params.items():
                sample_str += ('\n'+param + ': ' + str(val))
                contur.config.contur_log.debug(sample_str)

            if extract:
                # Here we are pulling out the histograms for a specific analysis
                # make the new directory if needed
                new_one = os.path.join(new_dir,root[len(scan_path)+1:])
                if not os.path.exists(new_one):
                    os.makedirs(new_one)
                # copy over the param file.
                shutil.copy(param_file_path,new_one)
                # do the extraction
                aos = yoda.read(merged_yoda_file_gz)
                for analysisName in patterns:
                    aos_new = []
                    for path, ao in aos.items():
                        if not rivet.isRawPath(path) and analysisName in path:
                            aos_new.append(ao)
                    yoda.write(aos_new, os.path.join(new_one, analysisName+".yoda"))

        if unmerge or archive or check or check_all:

            # get LHC-*.yoda(.gz) files
            file_list = []
            file_list_gz = []
            for name in files:
                conditions = (name.endswith('yoda')
                              and
                              name != 'LHC.yoda' and
                              'LHC' in name and
                              contur.config.tag in name)
                conditions_gz = (name.endswith('yoda.gz')
                                 and
                                 name != 'LHC.yoda.gz' and
                                 'LHC' in name and
                                 contur.config.tag in name)
                if conditions:
                    file_list.append(os.path.join(root, name))
                if conditions_gz:
                    file_list_gz.append(os.path.join(root, name))

                if name.startswith(contur.config.tag+run_point_num+".sh.e"):
                    got_batch_logs = True
                if name==contur.config.tag+run_point_num+".sh":
                    batch_script = name

            if file_list_gz and unmerge:

                # decompress LHC-*.yoda.gz files if necessary and remove any merged ones
                command = ' '.join(['gzip -d']+file_list)
                contur.config.contur_log.debug(command)
                os.system(command)
                # remove any merged files
                if os.path.exists(merged_yoda_file):
                    # remove merged file
                    os.remove(merged_yoda_file)
                if os.path.exists(merged_yoda_file_gz):
                    # remove merged file
                    os.remove(merged_yoda_file_gz)

            if archive:
                if got_merged_yoda_file:

                    # also removing the pre-merged yodas.
                    if file_list_gz:
                        for yfile_gz in file_list_gz:
                            os.remove(yfile_gz)
                    if file_list:
                        for yfile in file_list:
                            os.remove(yfile)

                # compress everything that isn't compressed, except the params.dat file
                for fname in files:
                    if not fname.endswith('.gz') and not fname==contur.config.paramfile:
                        # (need to check they weren't already deleted or zipped.)
                        name = os.path.join(root,fname)
                        if os.path.isfile(name) and not os.path.islink(name):
                            command = 'gzip {}'.format(name)
                            contur.config.contur_log.debug(command)
                            os.system(command)

            got_valid_yoda = file_list_gz or file_list or got_merged_yoda_file
            if (check or check_all) and not got_valid_yoda:
                # if there are batch job outputs, this is a failed job
                # otherwise it might still be running; if check_all is set, assume it failed to submit
                if got_batch_logs or (check_all and batch_script):
                    contur.config.contur_log.warn("Found a failed job: {}".format(root))
                    if resub and batch_script and not contur.config.using_condor:
                        # if there is a script and the flag is setup, resubmit
                        with contur.scan.WorkingDirectory(root):
                            contur.config.contur_log.info("Resubmitting {}".format(batch_script))
                            qsub = gen_submit_command(queue)

                            subprocess.call([qsub + " " +batch_script],
                                            shell=True)
