#!/usr/bin/env python
import os
import pickle
import re
import shutil

import numpy as np
from contur.scan import make_directory, make_run_point_directory, write_param_dat, write_template_files, \
    write_sampled_map

from .os_functions import *
import contur


def read_template_file():
    """Read in template files and store contents in dictionary"""
    template = {}
    with open(contur.config.mceg_template, 'r') as f:
        template[contur.config.mceg_template] = f.read()
    return template


def check_param_consistency(param_dict, template_path):
    """Check that parameters in param file match those in templates."""

    with open(template_path, 'r') as template_file:
        template_text = template_file.read()

    from string import Formatter
    template_param_names = set([fn for _, fn, _, _ in Formatter().parse(template_text) if fn is not None])
    param_names = set(param_dict.keys())

    # Check that there aren't unknown params in the template
    if not template_param_names.issubset(param_names):
        unknown = template_param_names - param_names
        print("Error: unknown parameters {} in template file {}".format(list(unknown), template_path))
        # TODO: it'd be better to raise this as an exception, so the calling code can take  evasive action,
        #   e.g. cleaning up aborted scan dirs. Usually a good plan that only top-level code calls exit()
        sys.exit(1)

    # Check that all param names are used: warn but not exit if not
    if template_param_names < param_names: #< i.e. proper subset
        unused = param_names - template_param_names
        print("Warning: parameters {} unused in template file {}".format(list(unused), template_path))


def sample_fromvalues(param_dict, num_points):
    """for read mode we return the grid by having already worked out each axis (allows us to mix LIN and LOG)"""
    dimensions = len(param_dict)
    points_per_dim = round(num_points ** (1. / dimensions))
    if not np.isclose(points_per_dim, num_points ** (float(1. / dimensions))):
        permission_message = ("If using uniform mode number of points^"
                              "(1/dimensions) must be an integer!\n"
                              "Do you want to use %i points?"
                              % points_per_dim ** dimensions)
        if not permission_to_continue(permission_message):
            sys.exit()
    grid = np.meshgrid(*[param["axes"] for param in param_dict.values()])
    coords = [np.reshape(dim, dim.size) for dim in grid]
    return coords


def read_old_points(map_file):
    """Read coordinates of points sampled in .map file"""
    with open(map_file, 'rb') as f:
        depots = pickle.load(f)

    points = []
    for depot in depots:
        points.append([depot.params[param] for param in sorted(depot.params)])
    old_points = np.array(points)

    return old_points


def coords_from_axes(param_dict):
    """Read the param_dict that has been dressed with axes values and build a grid to return"""
    grid = np.meshgrid(*[v['axes'] for v in param_dict.values()])
    coords = [np.reshape(dim, dim.size) for dim in grid]
    return coords


def generate_points(param_dict):
    """
    Generate points to sample using given mode.

    Parameters
    ----------

    param_dict: dict
        Dictionary with parameter names as keys each containing another
        dictionary with keys 'range' and 'values'.

     Returns
     -------

     param_dict: dict
        Same param_dict as was passed in to the function but now filled
        with points.

    num_points: int
        The number of points generated.

    """

    dimensions = len(param_dict)

    for k, v in sorted(param_dict.items()):
        # this is a bit of hybrid mess, shoudl just make the grid directly out of these axes and attach to object I think?....
        if v["mode"] == "LOG":
            # quick work to make the file read log spacing
            v["axes"] = contur.util.newlogspace(v["start"], v["stop"], v["number"])
        elif v["mode"] == "LIN":
            v["axes"] = np.linspace(v["start"], v["stop"], v["number"])
        elif v["mode"] == "CONST":
            v["axes"] = v["value"]
        elif v["mode"] == "REL":
            # for now just give REL a dummy 0 axis
            v["axes"] = 0.0
        elif v["mode"] == "DIR":
            v["axes"] = 0.0
            scan_dir = v["name"]
            if os.path.isdir(scan_dir):
                v["axes"] = os.listdir(scan_dir)
            else:
                print("%s is not a directory" % scan_dir)
        elif v["mode"] == "SINGLE":
            # single SLHA file
            v["axes"] = v["name"]

        else:
            raise ValueError(
                "Unrecognised mode parameter %s in param_card.dat " % v["mode"])

    coords = coords_from_axes(param_dict)

    for idx, param in enumerate(param_dict):
        # copy the coords once so all the explicitly set parameters are available in param_dict
        param_dict[param]['values'] = coords[idx]

    for idx, param in enumerate(param_dict):
        # This is sloppy but we need all params available before trying to find the relative parameters
        # now we can work out what REL should be
        if param_dict[param]["mode"] == "REL":
            #form = param_dict[param]["form"]
            for id, val in enumerate(coords[idx]):
                form = param_dict[param]["form"]
                while re.search(r"\{(.*?)\}", form):
                    matches = re.search(r"\{(.*?)\}", form)
                    # clean the parameters out with their numeric values one by one
                    try:
                        form = re.sub(matches.group(0), str(
                            param_dict[matches.group(1)]['values'][id]), form)
                    except:
                        KeyError(
                            "Could not find defined parameter %s to substitute in form" % param_dict[param])

                try:
                    #coords[idx][id]='%.5f' % eval(form)
                    coords[idx][id] = eval(form)
                except:
                    ValueError("Could not evaluate form %s for parameter %s" % (
                        form, param_dict[param]))

            param_dict[param]['values'] = coords[idx]

    num_points = len(coords[0])
    return param_dict, num_points

def run_scan(param_dict, beams, num_points, args, exclusions = []):
    """
    Given points defined in param_dict run create run point directories
    and populate with MCEG template file and parameter file.

    Parameters
    ----------

    param_dict: dict
        Dictionary with parameter names as keys each containing another
        dictionary with keys 'range' and 'values'.

    args: argparse.Namespace object
        Argparse object with attributes containing command line options.

    Returns
    -------

    None

    """

    # Read in run card template files
    template = read_template_file()

    make_directory(args.out_dir)

    for beam in beams:
        directory = args.out_dir+"/"+beam
        make_directory(directory)

        # TODO: move this wrapper to a common contur util area
        try:
            from tqdm import tqdm as pbar
        except ImportError:
            def pbar(iterable, **kwargs): return iterable

        for run_point in pbar(range(num_points), desc="Building scripts for "+beam):
            # Skip this point if it is excluded in param file
            if run_point in exclusions:
                continue


            # Run point directories are inside the output directory and hold
            # the necessary files to run the generator with the param_dict associated
            # with that point
            run_point_path = make_run_point_directory(run_point, directory)

            # Write run card template files formatted with parameter values
            write_template_files(template, param_dict, run_point,
                                 run_point_path, args, beam)

            # Write all sampled points and their run points to a .dat file
            write_map = True
            if "slha_file" in param_dict.keys():

                import pyslha

                if param_dict['slha_file']['mode'] == "DIR":
                    # copy the SLHA file over
                    info = param_dict['slha_file']
                    f_source = os.path.join(
                        param_dict['slha_file']['name'], info['values'][run_point])
                    f_dest = os.path.join(
                        os.getcwd(), run_point_path, info['values'][run_point])
                    shutil.copyfile(f_source, f_dest)
                    write_map = False

                elif param_dict['slha_file']['mode'] == "SINGLE":
                    # read the SLHA, modify it and write it out.
                    slha_name = param_dict['slha_file']['name']
                    susy = pyslha.read(slha_name)
                    # now loop over the parameters to apply any changes.
                    for param, info in sorted(param_dict.items()):

                        # values are scaled relative to the default in te SLHA file
                        if param != 'slha_file':
                            print(param)
                            try:
                                for ident in susy.blocks[param].keys():
                                    slha_val = susy.blocks[param][ident]
                                    susy.blocks[param][ident] = slha_val * \
                                        info['values'][run_point]
                            except:
                                print("SLHA BLOCK not found:"+param)

                    f_dest = open(os.path.join(
                        os.getcwd(), run_point_path, slha_name), "w")
                    pyslha.write(f_dest, susy)

            if write_map:
                write_sampled_map(directory, param_dict)

            # Write parameter file inside run point directory. This is purely to
            # record what the param_dict are at this run pointg
            #write_param_file(param_dict, run_point_path, run_point)
            write_param_dat(param_dict, run_point_path, run_point)
