import sys, os
import logging
import contur.config

def isValidYodafile(file):
    valid=True
    if not os.path.exists(file):
        valid = False

    return valid

def read_out_file(root,files,matrix_elements):
    """ Find the Herwig .out file and read various values from it"""
    import re
    me_dict = {}

    for test_name in files:
        if test_name.endswith('.out'):
            out_file_path = os.path.join(root, test_name)

#            with open(out_file_path, 'r') as out_file:
#                for line in out_file.readlines():
#                    for me in matrix_elements:
#                        if not me in me_dict and me in line:
#                            values = line.split()
#                            # dicking around to remove the error surrounded by brackets
#                            num = values[3].split('(')[0]
#                            exp = values[3].split(')')[1]
#                            me_dict[me] = float(num+exp)
#                            print me_dict[me]
#                        break

            for me in matrix_elements:
                with open(out_file_path, 'r') as out_file:
                    mename, mepatt = me, me
                    if "=" in me:
                        mename, mepatt = me.split("=")
                    thisxsec = 0.0
                    for line in out_file.readlines():
                        values = line.split()
                        if values and (values[0] == mepatt or re.match(mepatt, values[0])):
                            #if re.match('^ME.*(tptp~|bpbp~|xx~|yy~).*', values[0]):
                            print(line)
                            #print mepatt
                            if values[3] != '0':
                                # dicking around to remove the error surrounded by brackets
                                num = values[3].split('(')[0]
                                exp = values[3].split(')')[1]
                                thisxsec = float(num+exp)
                                print(thisxsec)
                            else:
                                thisxsec = 0.0
                            me_dict.setdefault(mename, 0.0)
                            me_dict[mename] += thisxsec
                            print(me_dict.get(mename))
                    print(me_dict.get(mename))
    return me_dict

 
def read_log_file(root,files,branching_fractions,widths,masses):
    """ Find the Herwig .log file and read various values from it"""
    bf_dict = {}
    w_dict = {}
    m_dict = {}

    for test_name in files:
        if test_name.endswith('.log'):
            log_file_path = os.path.join(root, test_name)
            with open(log_file_path, 'r') as log_file:
                for line in log_file.readlines():
                    if branching_fractions is not None:
                        for bf in branching_fractions:
                            bf_store = "AUX:"+bf
                            if not bf==" "  and not bf_store in bf_dict and bf in line:
                                values = line.split()
                                print(values)
                                bf_dict[bf_store] = values[2]
                                print(bf_dict[bf_store])

                    if widths is not None:
                        for w in widths:
                            w_store = "AUX:"+w
                            if not w==" " and not w_store in w_dict and w in line:
                                values = line.split()
                                print(values)
                                lv = len(values)
                                w_dict[w_store] = values[lv-1]
                                print(w_dict[w_store])

                    if masses is not None:
                        for m in masses:
                            m_store = "AUX:"+m
                            if "Parent" in line and not m==" " and not m_store in m_dict and m in line:
                                values = line.split()
                                print(values)
                                lv = len(values)
                                m_dict[m_store] = values[lv-5]
                                print(m_dict[m_store])


    bf_dict.update(w_dict)
    bf_dict.update(m_dict)
    return bf_dict


def read_slha_file(root,files,block_list):
    """ read particle masses from an SLHA1 file (if found) """
    import pyslha
    mass_dict = {}

    for test_name in files:
        
        if test_name.endswith('.slha1'):
            shla_file_path = os.path.join(root, test_name)
            slha_file = open(shla_file_path, 'r')
            d = pyslha.read(slha_file)
            for block in block_list:
                for k, v in d.blocks[block].items():
                    if contur.config.binwidth > 0:
                        mass_dict[str(k)]=(contur.config.binoffset+contur.config.binwidth*int(abs(v)/contur.config.binwidth))
                    else:
                        mass_dict[str(k)]=abs(v)  
                    
                      
    return mass_dict



def read_param_point(file_path):
    """Read a parameter file and return dictionary of contents"""
    with open(file_path, 'r') as param_file:
        raw_params = param_file.read().strip().split('\n')

    param_dict = {}
    for param in raw_params:
        name, value = param.split(' = ')
        param_dict[name] = float(value)

    return param_dict

