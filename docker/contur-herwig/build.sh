#! /usr/bin/env bash

set -e

RIVET_VERSION=3.1.2
HERWIG_VERSION=7.1.2
CONTUR_VERSION=master #1.2.0

MSG="Building contur-herwig with Rivet=$RIVET_VERSION, Herwig=$HERWIG_VERSION"
tag="hepstore/contur-herwig:$CONTUR_VERSION-$HERWIG_VERSION"

docker build . -f Dockerfile -t $tag

docker tag $tag hepstore/contur-herwig:$CONTUR_VERSION
docker tag $tag hepstore/contur-herwig:latest

if [[ "$PUSH" = 1 ]]; then
     docker push $tag && sleep 30s
     docker push hepstore/contur-herwig:$CONTUR_VERSION && sleep 30s
     docker push hepstore/contur-herwig:latest
fi
