cd /contur/AnalysisTools/contur/bin/

touch $1/processes.txt

./contur-extract-herwig-xs-br --ws --xy $2,$3 -i $1 > $1/processes.txt

sed -i '/^$/d' $1/processes.txt