# CONTUR Visualiser

## About the Visualiser

This visualiser presents an easy-to-use interface to view data from your CONTUR runs. For now, the visualiser supports heatmaps, scatterplots and contour plots for the dataset provided.

## Installation and Setup

You can skip the docker setup if you have CONTUR installed on your system with the right configurations as the associated Dockerfile. 

### Docker Setup

The docker container in /contur/docker/contur-visualiser is updated. Please use that for building.
Note: Please use sudo if your docker group is not running in superuser mode.

1. Build the docker image
```
cd /contur/docker/contur-visualiser
docker build -t contur-website .
```
This image mounts CONTUR at /contur, and the visualiser would be found at /contur/contur-visualiser. The process usually takes about 30 mins, but this will be fixed in later releases using a minimal CONTUR image.

2. After building is successful, run the container, exposing the port of it to 8080
```
docker run -it -p 80:80 -v {$DATASET_DIR}:/dataset contur-website
```

You should be greeted with:
```
Successfully found tqdm python module
Successfully found numpy python module
Successfully found YODA python module
Successfully found Rivet python module
Successfully found scipy python module
Successfully found configobj python module
Successfully found sqlite3 python module
Successfully found matplotlib python module
Successfully found pyslha python module

All python module dependencies appear to be accessible.

---------------------------------------------
Environment to run Contur successfully set up
---------------------------------------------
```

### Running the Website

1. Start the webserver:
```
cd contur-visualiser
./contur-visualiser -d {$DATASET_DIR} -w[OPTIONAL] {$WEBSITE_DIR} -x {X} -y {Y} -m <contur.map path>
```
Here, the DATASET_DIR would be of the form .../13TeV, ../7TeV or any other energy configuration. The WEBSITE_DIR points by default to this directory, so it can be omitted. However, you can use your own copy of the website to add new developments. The X and Y variables denote the variables used for graphing, and they should be valid according to the depickled map file, specified in contur.map.
The map file can be placed anywhere on the system, but the associated dataset must contain the same variables.
 
You can safely ignore all errors and warnings. Please omit any trailing '/' characters in the options.
A sample would be
```
cd contur-visualiser

./contur-visualiser -d /dataset/cedar/lcorpe/contur_271219_rivet3corrs/contur/run-area-TFHM/myscan_granular_lin_tail_30k_HDMYfix3/13TeV -w /contur/contur-visualiser[OPTIONAL] -x mzp -y tsb -m /dataset/cedar/lcorpe/contur_271219_rivet3corrs/contur/run-area-TFHM/myscan_granular_lin_tail_30k_HDMYfix3/13TeV/Analysis/contur.map
```

2. Visit 0.0.0.0 in your browser, which will show a HTML webpage
3. As a sanity check, test whether there are the following files in the directory structure
```
Folder data has excl.txt, xcoords.txt, ycoords.txt, zcoords.txt
Folder contur-website has conturPlot/ and contur.log
```

4. If you get errors such as
```
convert-im6.q16: attempt to perform an operation not allowed by the security policy `PS' @ error/constitute.c/IsCoderAuthorized/408.
convert-im6.q16: no images defined `d09-x01-y01.png' @ error/convert.c/ConvertImageCommand/3258.
```

Do
```
sudo nano /etc/ImageMagick-8/policy.xml
Add <policy domain="coder" rights="read | write" pattern="PS" />
```

Additionally, after your run, please ensure these directories are deleted if you plan to use a new dataset. This is because the server can be terminated only with `Ctrl + C`, which doesn't erase those dependencies.
```
/contur/contur-visualiser/data/
/contur/contur-visualiser/conturPlot/
/contur/contur-visualiser/contur.log
/contur/contur-visualiser/templates/point
/contur/contur-visualiser/static/point
```
## Options available

You can pass the following arguments to the contur-visualiser executable
1. -m : Full path to the contur.map file
2. -x : Name of your 'x' variable
3. -y : Name of your 'y' variable
4. -d : Dataset directory without trailing backslashes
5. -w : (Optional)Website directory without trailing backslashes

Clicking any points will generate the plots for that point and open in a new tab the html spec of that point. Please wait upto 10 minutes for the plotting to complete. Previously plotted points will be cached, and will open almost immediately.

## Component Interaction

<img src = "contur-visualiser/static/gifs/contur-terminal.gif" />

The executable contur-visualiser defines a Flask environment on the local system to service the dataset passed as the argument. When the dataset is parsed, the following steps occur:
- If the data folder is not made, the X, Y and Z coordinates are imported from the map file using the script run.sh
- Next, the script extract_plots.sh extracts the processes.txt files for all the points, fetching metadata about the points
- The Flask service then plots this onto 0.0.0.0:80 for viewing.

<img src = "contur-visualiser/static/gifs/contur-website.gif" />

The Flask environment provides a lot of features to visualise the data well, with the help of the JavaScript graphing library, Plotly.js.
- When a user hovers over a point, the point metadata is displayed right below the graph.
- When the user clicks on a point, in a few moments, the user is redirected to the point-specific index page. This may take some time as the YODA tool parses the information on the fly. However, if the point is visited before, it will load instantaneously.

## Contributing

Refer to https://gitlab.com/hepcedar/contur for up-to-date documentation on how to contribute to the CONTUR project.