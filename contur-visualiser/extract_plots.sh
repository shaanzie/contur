rm -r templates/point static/point
mkdir -p $2/templates/point/
mkdir -p $2/static/point/
cd $1
contur *yoda*
contur-mkflask --rp *yoda*
cp -r $1/contur-plots/ $2/templates/point/
cp -r $1/contur-plots/ $2/static/point/
