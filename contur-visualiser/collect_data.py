import os
import pandas as pd

def send_text(filename):
    text_arr = list()
    filepath_arr = list()
    for subdir, dirs, files in os.walk(filename):
        for file in files:
            filepath = subdir + os.sep + file
            if('contur-plot' not in filepath and 'plots' not in filepath and 'ANA' not in filepath and filename != filepath):
                if filepath.endswith("processes.txt"):
                    filepath_arr.append(filepath)
    filepath_arr.sort()
    for filepath in filepath_arr:
        textfile = open(filepath, "r")
        lines=textfile.readlines() 
        if(len(lines)):
            temp = ""
            for line in lines:
                temp += line + "<br>"
            text_arr.append(temp)
    
    return text_arr

# def parse_objects():
    
#     points = list()
#     with open('data/excl.txt') as f:
#         points = eval(f.readline())
#     return points

def send_x(x):
    try:
        df = pd.read_csv("data/excl.csv")
        return [str(i) for i in list(df[x])]
    except:
        return None

def send_y(y):
    try:
        df = pd.read_csv("data/excl.csv")
        return [str(i) for i in list(df[y])]
    except:
        return None

def send_z():

    try:
        df = pd.read_csv("data/excl.csv")
        return [str(i) for i in list(df['CLs'])]
    except:
        return None

def send_links(dataset):
    text_arr = list()
    
    for subdir, dirs, files in os.walk(dataset):
        for file in files:
            filepath = subdir + os.sep + file
            
            if filepath.endswith("contur-plots/index.html"):
                text_arr.append(filepath)
    return text_arr

def send_links_notmade(dataset):
    text_arr = list()
    for dir in os.listdir(dataset):
       filepath = dataset + os.sep + dir
       if os.path.exists(filepath+os.sep+"contur-plots"):
           text_arr.append(filepath)
    return text_arr
