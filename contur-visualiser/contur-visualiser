#!/usr/bin/python3

from flask import Flask, render_template, jsonify, request, abort, session, redirect, url_for, Response
import json.encoder
from flask_cors import CORS
import json
import pandas as pd
from flask import jsonify
from collect_data import send_text, send_x, send_y, send_z, send_links, send_links_notmade
import os
import sys
import time
import optparse

parser = optparse.OptionParser(usage="%prog [options]")
parser.add_option("-d", "--dataset", help='Path to dataset without trailing backspace', dest='_dataset')
parser.add_option("-m", "--map", help='Path to contur.map file', dest='_mapfile')
parser.add_option("-w", "--website", help='Path to website directory without trailing backspace', dest='_website', default="/contur/contur-visualiser")
parser.add_option("-x", "--xValue", help='X coordinate', dest='_xValue')
parser.add_option("-y", "--yValue", help='Y coordinate', dest='_yValue')

(options, args) = parser.parse_args()

if not options._mapfile:
    parser.error('Map file not given')

if not options._website:
    parser.error('Website dir not given')

if not options._dataset:
    parser.error('Dataset not given')

if not options._xValue:
    parser.error('X not given')

if not options._yValue:
    parser.error('Y not given')

app = Flask(__name__)


import logging
log = logging.getLogger('werkzeug')
# log.setLevel(logging.ERROR)


CORS(app)

@app.route("/")
def home():
    return render_template("home.html")

@app.route("/collect_x",  methods=['GET'])
def collect_x():
    x = send_x(x=options._xValue)
    if(x is None):
        print(str(options._xValue) + " not in " + str(options._mapfile))
        func = request.environ.get('werkzeug.server.shutdown')
        if func is None:
            raise RuntimeError('Not running with the Werkzeug Server')
        func()
        exit(0)
    return ";".join(x)

@app.route("/collect_y",  methods=['GET'])
def collect_y():
    y = send_y(y=options._yValue)
    if(y is None):
        print(str(options._yValue) + " not in " + str(options._mapfile))
        func = request.environ.get('werkzeug.server.shutdown')
        if func is None:
            raise RuntimeError('Not running with the Werkzeug Server')
        func()
        exit(0)
    return ";".join(y)

@app.route("/collect_z",  methods=['GET'])
def collect_z():
    z = send_z()
    return ";".join(z)

@app.route("/collect_text",  methods=['GET'])
def collect_text():
    text = send_text(options._dataset)
    for i in range(len(text)):
        text[i] = text[i].replace("\n", "<br>")
    return ";".join(text)

@app.route("/collect_links",  methods=['GET'])
def collect_links():
    links = send_links_notmade(options._dataset)
    return ";".join(links)

@app.route("/make_plots",  methods=['POST'])
def make_plots():
    data = request.get_data().strip()
    foundfile = os.path.isfile(str(data.decode('utf-8'))+os.sep+"contur-plots/index.html")
    if not os.path.isfile(str(data.decode('utf-8'))+os.sep+"contur-plots/index.html"):
      print("bash extract_plots.sh " + str(data.decode('utf-8')) + " " + options._website)
      os.system("bash extract_plots.sh " + str(data.decode('utf-8')) + " " + options._website)
    
    print("bash movefile.sh " + str(data.decode('utf-8')) + " " + options._website)
    os.system("bash movefile.sh " + str(data.decode('utf-8')) + " " + options._website)
                            
    return "Success"

@app.route("/index")
def index():
    return render_template('point/index.html')

@app.route("/xlabel")
def xlabel():
    return options._xValue

@app.route("/ylabel")
def ylabel():
    return options._yValue



if __name__=='__main__':
    
    clearCache=1
    hasX = os.path.isfile(options._website+os.sep+"data"+os.sep+"xcoords.txt")
    hasY = os.path.isfile(options._website+os.sep+"data"+os.sep+"ycoords.txt")
    hasZ = os.path.isfile(options._website+os.sep+"data"+os.sep+"zcoords.txt")
    if not (hasX and hasY and hasZ):
      res = os.system("bash run.sh " + options._website + " " + options._dataset + " " + options._xValue + " " + options._yValue + " " + options._mapfile)
      if(res != 0):
        exit(0)
    cc=-1
    dirs=sorted(os.listdir(options._dataset))
    for _dir in dirs:
        cc+=1
        ext = options._dataset + os.sep + _dir
        if("static" in ext): continue
        if cc%10==0: print("%d/%d : %s"%(cc,len(dirs), ext))
        if not os.path.isdir(ext): continue
        if "plot" in ext: continue
        if clearCache: os.system("rm %s"%(ext+os.sep+"processes.txt"))
        if not os.path.exists(ext+os.sep+"processes.txt"):
          os.system('bash scripts/extract_params.sh ' + ext + " " + options._xValue + " " + options._yValue)
    clearCache=0


    if(options._website and options._dataset and options._xValue and options._yValue):
        app.run(host='0.0.0.0', port='80', debug=True, threaded=True)
